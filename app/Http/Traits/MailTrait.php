<?php

namespace App\Http\Traits;


use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

trait MailTrait
{
    public function sendMail($to, $msg){
        require base_path("vendor/autoload.php");
        $mail = new PHPMailer(true);     // Passing `true` enables exceptions
        try {

            // Email server settings
            $mail->SMTPDebug = 0;
            $mail->isSMTP();
            $mail->Host = 'duaacollection.com';             //  smtp host
            $mail->SMTPAuth = true;
            $mail->Username = 'info@duaacollection.com';   //  sender username
            $mail->Password = 'Tread@12345';       // sender password
            $mail->SMTPSecure = 'tls';                  // encryption - ssl/tls
            $mail->Port = 587;                          // port - 587/465

            $mail->setFrom('info@duaacollection.com', 'Khuram');
            $mail->addAddress($to);
//            $mail->addCC($request->emailCc);
//            $mail->addBCC($request->emailBcc);

            $mail->addReplyTo('khuram.developer@gmail.com', 'Khuram');


            $mail->isHTML(true);                // Set email content format to HTML

            $mail->Subject = 'Invoice APP';
            $mail->Body = $msg;

            // $mail->AltBody = plain text version of email body;

            if (!$mail->send()) {
                $res['status'] = 'failed';
                $res['error'] = $mail->ErrorInfo;
                return response()->json($res);
            } else {
                $res['status'] = 'success';
                $res['error'] = 'Email has been sent.';
                return response()->json($res);
            }

        } catch (Exception $e) {
            $res['status'] = 'error';
            $res['error'] = 'Message could not be sent' . $e->errorMessage();
            return response()->json($res);
        }
    }
}


?>