<?php
namespace App\Http\Traits;

use App\Models\system_logs;
use Illuminate\Support\Facades\Session;

trait LogsTrait{

    public function add_log($name, $detail, $table, $table_id){
        $user = Session::get('user');
        $action = new system_logs();

        $action->actions = $name;
        $action->action_detail = $detail;
        $action->user_id = $user->id;
        $action->table = $table;
        $action->table_id = $table_id;
        $action->save();
        return true;
    }


}



?>
