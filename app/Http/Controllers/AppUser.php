<?php

namespace App\Http\Controllers;

use App\Http\Traits\LogsTrait;
use App\Models\AppUsers;
use Illuminate\Http\Request;
use App\Models\PartyModel;

class AppUser extends Controller
{
    use LogsTrait;
    public function ViewParty(){
        $users = AppUsers::all();
        return view('admin.appUser.index')->with('users', $users);
    }

    public function delete_app_user($id){
        AppUsers::where('id', $id)->delete();
        $this->add_log('delete', 'Delete app user', 'app_users', $id);
        return redirect('/appUser/');
    }

    public function view_detail($id){
        $user = AppUsers::where('id', $id)->first();
        return view('admin.appUser.app_user_view')->with('user', $user);
    }

    public function update_detail(Request $req){
        $user = AppUsers::where('id', $req->user_id)->first();
        $user->f_name = $req->f_name;
        $user->l_name = $req->l_name;
        $user->phone = $req->phone;
        $user->email = $req->email;
        $user->address = $req->address;
        $user->city = $req->city;
        $user->zip = $req->zip;
        $user->state = $req->state;
        $user->country = $req->country;
        $user->verified = $req->verified;
        if(isset($user->bank_detail)){
            $bankDetail['bank_name'] = $req->bank_name;
            $bankDetail['account_no'] = $req->account_no;
            $bankDetail['short_code'] = $req->short_code;
            $bankDetail['bank_address'] = $req->bank_address;
            $bankDetail['bank_city'] = $req->bank_city;
            $bankDetail['bank_state'] = $req->bank_state;
            $bankDetail['bank_zip'] = $req->bank_zip;
            $bankDetail['bank_country'] = $req->bank_country;
            $bankDetail['bank_phone_no'] = $req->bank_phone;
            $user->bank_detail = json_encode($bankDetail);
        }
        $user->save();
        $this->add_log('update', 'Update app user detail', 'app_users', $req->user_id);
        return redirect('/appUser/view_detail/'.$req->user_id);
    }



}
