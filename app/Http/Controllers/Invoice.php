<?php

namespace App\Http\Controllers;

use App\Http\Traits\LogsTrait;
use App\Models\Invoice as InvoiceModel;
use App\Models\AppUsers;
use App\Models\InvoiceAmount;
use Illuminate\Http\Request;


class Invoice extends Controller
{
    use LogsTrait;

    public function allInvoices(){
        $pending = InvoiceModel::where('invoice_status', 'pending')->get();
        $complete = InvoiceModel::where('invoice_status', 'complete')->get();
        $disputes = InvoiceModel::where('invoice_status', 'dispute')->get();
        $processing = InvoiceModel::where('invoice_status', '!=', 'pending')
            ->where('invoice_status', '!=', 'complete')
            ->where('invoice_status', '!=', 'dispute')->get();
        return view('admin.invoice.index')->with('pending', $pending)->with('complete', $complete)
            ->with('processing', $processing)->with('disputes', $disputes);
    }

    public function ViewInvoice($id){
        $invoice = InvoiceModel::where('id', $id)->first();
        $gsd = json_decode($invoice->invoice_gs_detail);
        if($invoice->invoice_type == 'user') {
            $userDetail = AppUsers::where('id', $invoice->app_user_id)->first();
            $invoiceUser = $userDetail;
            $invoiceUser->full_name = $invoiceUser->f_name.' '.$invoiceUser->l_name;
            $bankDetail = json_decode($userDetail->bank_detail);
        }else{
            $invoiceUser = json_decode($invoice->guest_user_detail);
            $bankDetail = json_decode($invoice->guest_bank_detail);
        }
        $invoiceAmount = InvoiceAmount::where('invoice_id', $invoice->id)->get();
        return view('admin.invoice.view_invoice')->with('invoice', $invoice)->with('user', $invoiceUser)
            ->with('bankDetail', $bankDetail)->with('gsd', $gsd)->with('invoiceAmount', $invoiceAmount);
    }

    public function payment(){
        $pending = InvoiceAmount::where('verified', 0)->get();
        $complete = InvoiceAmount::where('verified', 1)->get();
        return view('admin.invoice.payment')->with('pending', $pending)->with('complete', $complete);
    }

    public function approve_payment($id){
        $payment = InvoiceAmount::where('id', $id)->first();
        $payment->verified = 1;
        $this->add_log('update', 'Approve payment', 'invoice_amount', $id);
        $payment->save();
        return redirect('invoice/payment');
    }

    public function delete_payment($id){
        $payment = InvoiceAmount::where('id', $id)->delete();
        $this->add_log('delete', 'Delete payment', 'invoice_amount', $id);
        return redirect('invoice/payment');
    }

    public function delete_invoice($id){
        InvoiceModel::where('id', $id)->delete();
        $this->add_log('delete', 'Delete invoice', 'invoice', $id);
        return redirect('/invoice/');
    }

    public function update_status($id, $status){
        $invoice = InvoiceModel::where('id', $id)->first();
        $invoice->invoice_status = $status;
        $this->add_log('update', 'update invoice status ('.$status.')', 'invoice', $id);
        $invoice->save();
        return redirect('invoice/viewInvoice/'.$id);
    }

    public function invoice_dispute_detail($id){
        $invoice = InvoiceModel::where('id', $id)->first();
        return view('admin.invoice.dispute_view')->with('invoice', $invoice);
    }
    public function add_dispute_notes(Request $req){
        $invoice = InvoiceModel::where('id', $req->id)->first();
        $dispute = json_decode($invoice->dispute_detail);
        $dispute->notes = $req->notes;
        $invoice->dispute_detail = json_encode($dispute);
        $invoice->save();
        return redirect('invoice/dispute_detail/'.$req->id);
    }

}
