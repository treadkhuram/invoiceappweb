<?php

namespace App\Http\Controllers;

use App\Models\system_logs;
use Illuminate\Http\Request;

class SystemLogs extends Controller{
    public function index(){
        $logs = system_logs::get();
        return view('admin.logs')->with('logs', $logs);
    }

    public function delete_log($id){
        if($id == -1){
            system_logs::query()->delete();
        }else{
            system_logs::where('id', $id)->delete();
        }
        return redirect('logs');
    }
}
