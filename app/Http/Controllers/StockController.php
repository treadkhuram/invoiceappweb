<?php

namespace App\Http\Controllers;

use App\Models\StockModel;
use Illuminate\Http\Request;

class StockController extends Controller
{
    public function ViewStock(){
        return view('admin.stock.index');
    }
    public function AddStock(Request $request){
        $postedData = $request->all();
        if(count($postedData) >= 1){
            $store = new StockModel();
            $store->fill($postedData);
            $store->save();
            return back()->with('success','Data has been Store Successfully');
        }
        return view('admin.stock.add');
    }
}
