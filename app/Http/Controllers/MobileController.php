<?php

namespace App\Http\Controllers;

use App\Models\AppUsers;
use App\Models\ContactUS;
use App\Models\Disputes;
use App\Models\InvoiceAmount;
use Illuminate\Http\Request;
use App\Http\Traits\MailTrait;
use App\Models\Invoice;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use PDF;

class MobileController extends Controller{

    use MailTrait;

    public function check(){
        return response()->json('hello world');
    }

    public function create_account(Request $req){
        $user = new AppUsers();
        $user->fill($req->all());
        $findUser = AppUsers::where('email',$req->email)->first();
        if($findUser == null) {
            $user->password = md5($req->password);
            $user->verified_token = uniqid();
            $user->save();
            $msg = 'Kindly click on the following link to verify you account ' . url('/') . '/verify_user/' . $user->verified_token;
                    $this->sendMail($user->email, $msg);
            $res['status'] = 'success';
            $res['msg'] = 'Account created, Kindly verify you email';
            return response()->json($res);
        }
        $url = url('/') . '/verify_user/' . $findUser->verified_token;
        $link = '<a target="_blank" rel="noopener noreferrer" href="'.$url.'">Click here</a>';

        $msg = 'Kindly click on the following link to verify you account '.$link;

        $this->sendMail($user->email, $msg);
        $res['status'] = 'error';
        $res['msg'] = 'Account already exist, Kindly login.';
        return response()->json($res);
    }

    public function login(Request $req){
        $user = AppUsers::where('email',$req->email)
            ->where('password',md5($req->password))->first();
        if($user == null){
            $res['status'] = 'error';
            $res['msg'] = 'Invalid email or password';
            return response()->json($res);
        }else if($user->verified == 0){
            $res['status'] = 'error';
            $res['msg'] = 'Kindly verify you email address';
            $msg = 'Kindly click on the following link to verify you account ' . url('/') . '/verify_user/' . $user->verified_token;
            $this->sendMail($user->email, $msg);
            return response()->json($res);
        }
        $res['status'] = 'success';
        $res['user'] = $user;
        return response()->json($res);
    }

    public function verify_user($id=""){
        $id;
        if($id!=""){
            $findUser = AppUsers::where('verified_token',$id)->first();
            if(isset($findUser)){
                $findUser->verified = 1;
                $findUser->save();
                echo '<h4>Account verified successfully.</h4>';
                return;
            }
            echo '<h4>Error in account verification, kindly contact with admin.</h4>';
            return;
        }
        echo '<h4>Error in account verification, kindly contact with admin.</h4>';
        return;
    }

    public function create_invoice(Request $req){
        if(isset($req->invoice_id)){
            $invoice = Invoice::where('id', $req->invoice_id)->first();
        }else{
            $invoice = new Invoice();
        }
        $invoice->fill($req->all());
        if($req->invoice_for == 0){ //for goods
            $goods['brand_name'] = $req->brand_name;
            $goods['model_no'] = $req->model_no;
            $goods['serial_no'] = $req->serial_no;
            $goods['year_of_manufacture'] = $req->year_of_manufacture;
            $goods['product_condition'] = $req->product_condition;
            $goods['extra_link'] = $req->extra_link;
            $invoice->invoice_gs_detail = json_encode($goods);

            $imageArray = json_decode($req->image_array);
            $allImages = [];
            foreach ($imageArray as $item) {
                $imageData = $item->base64;
                $fileExtension = 'png';
                $imageName = Str::random(10) . '.' . $fileExtension;
                Storage::disk('public')->put($imageName, base64_decode($imageData));
                array_push($allImages, $imageName);
            }
            $invoice->images = json_encode($allImages);

        }else if($req->invoice_for == 1){ // for services
            $service['service_title'] = $req->service_title;
            $service['service_description'] = $req->service_description;
            $invoice->invoice_gs_detail = json_encode($service);
        }

        if(isset($req->guest_invoice) && $req->guest_invoice == 1){
            $userDetail['full_name'] = $req->user_full_name;
            $userDetail['address'] = $req->user_address;
            $userDetail['city'] = $req->user_city;
            $userDetail['state'] = $req->user_state;
            $userDetail['zip'] = $req->user_zip;
            $userDetail['country'] = $req->user_country;
            $userDetail['country_code'] = $req->user_country_code;
            $userDetail['phone'] = $req->user_phone;
            $userDetail['email'] = $req->user_email;
            $invoice->guest_user_detail = json_encode($userDetail);

            $bankDetail['e_email'] = $req->e_email;
            $bankDetail['e_phone'] = $req->e_phone;

            $bankDetail['bank_name'] = $req->bank_name;
            $bankDetail['account_no'] = $req->bank_account_no;
            $bankDetail['short_code'] = $req->bank_short_code;
            $bankDetail['bank_address'] = $req->bank_address;
            $bankDetail['bank_city'] = $req->bank_city;
            $bankDetail['bank_state'] = $req->bank_state;
            $bankDetail['bank_zip'] = $req->bank_zip;
            $bankDetail['bank_country'] = $req->bank_country;
            $bankDetail['bank_country_code'] = $req->bank_country_code;
            $invoice->guest_bank_detail = json_encode($bankDetail);
            $invoice->invoice_password = $req->invoice_password;
        }

        $invoice->save();
        $res['status'] = 'success';
        $res['msg'] = 'Invoice added successfully.';
        $res['html'] = Storage::disk('public')->url($this->getInvoicePdf($invoice->id));
//        $res['html'] = asset('storage/'.$this->getInvoicePdf($invoice->id));
        return response()->json($res);
    }

    public function getAllInvoices(Request $req){
        $userId = $req->user_id;
        $invoices = Invoice::where('app_user_id', $userId)->get();
        return response()->json($invoices);
    }

    public function getInvoiceNumber(){
        $inv = rand(1000, 10000000);
        $res['invoice_no'] = 'inv'.$inv;
        return response()->json($res);
    }

    public function searchInvoice(Request $req){
        $invNo = $req->invoice_no;
        $bankDetail = DB::table('bank_account')->first();
        if(isset($req->phone)){
            $invoices = Invoice::where('invoice_no', $invNo)->first();
            $phone = $req->phone;
            $invoices->amount_received = $this->getInvoiceAmount($invoices->id);
            if(isset($invoices) && $invoices->invoice_type == 'guest'){
                $userDetail = json_decode($invoices->guest_user_detail);
                if($userDetail->phone == $phone){
                    $res['status'] = 'success';
                    $res['invoice'] = $invoices;
                    $res['bank_detail'] = $bankDetail;
                    return response()->json($res);
                }else{
                    $res['status'] = 'error';
                    $res['msg'] = 'No record found.';
                    return response()->json($res);
                }
            }
            if(isset($invoices) && $invoices->invoice_type == 'user'){
                $userDetail = AppUsers::where('id', $invoices->app_user_id)->first();
                $invoices->invoice_user = $userDetail;
                if($userDetail->phone == $phone){
                    $res['status'] = 'success';
                    $res['invoice'] = $invoices;
                    $res['bank_detail'] = $bankDetail;
                    return response()->json($res);
                }else{
                    $res['status'] = 'error';
                    $res['msg'] = 'No record found.';
                    return response()->json($res);
                }
            }
        }else if(isset($req->password)){
            $invoices = Invoice::where('invoice_no', $invNo)->where('invoice_password', $req->password)->first();
        }
        else{
            $invoices = Invoice::where('invoice_no', $invNo)->first();
        }
        if(isset($invoices)) {
            if($invoices->invoice_type == 'user') {
                $userDetail = AppUsers::where('id', $invoices->app_user_id)->first();
                $invoices->invoice_user = $userDetail;
            }
            $invoices->amount_received = $this->getInvoiceAmount($invoices->id);
            $res['status'] = 'success';
            $res['invoice'] = $invoices;
            $res['bank_detail'] = $bankDetail;
            return response()->json($res);
        }else{
            $res['status'] = 'error';
            $res['msg'] = 'No record found.';
            return response()->json($res);
        }
    }

    public function getInvoiceAmount($id){
        $amount = InvoiceAmount::where('invoice_id', $id)->where('verified', 1)->sum('paid_amount');
        return $amount;
    }

    public function get_invoice_user($invoiceNo){

    }

    public function accept_reject_invoice(Request $req){
        $invoiceId = $req->invoice_id;
        $userId = $req->user_id;
        $acceptStatus = $req->accept_status;
        $invoice = Invoice::where('id', $invoiceId)->first();
        $invoice->invoice_status = $acceptStatus;
        $invoice->accepted_by = $userId;
        $invoice->accepted_date = date('y-m-d');
        $invoice->save();
        $res['status'] = 'success';
        $res['msg'] = 'Invoice accepted successfully.';
        return response()->json($res);

    }

    public function resetPasswordRequest(Request $req){
        $email = $req->email;
        $findUser = AppUsers::where('email',$email)->first();
        if(isset($findUser)){
            $findUser->verified = 1;
            $findUser->save();
            $res['status'] = 'success';
            $res['msg'] = 'kindly check you email address to reset password.';
            $msg = 'Kindly click on the following link to reset you password ' . url('/') . '/reset_password/' . $findUser->verified_token;
            $this->sendMail($findUser->email, $msg);
            $res['status'] = 'success';
            $res['msg'] = 'kindly check your email address to reset password.';
            return response()->json($res);
        }
        else{
            $res['status'] = 'error';
            $res['msg'] = 'Account not found, kindly sign up.';
            return response()->json($res);
        }
    }

    public function reset_password($id){
        return view('mobile_reset_password')->with('verified_token', $id);
    }

    public function update_password(Request $req){
        $userToken = $req->reset_token;
        $password = $req->new_password;
        $findUser = AppUsers::where('verified_token',$userToken)->first();
        $findUser->password = md5($password);
        $findUser->save();
        return redirect()->back()->with('message', 'Password updated successfully.');
    }


    public function composeEmail(){
        return $this->sendMail('khuramkhan344@gmail.com', 'hello ali');
    }

    public function create_payment(Request $req){
        $invoiceAmount = new InvoiceAmount();
        $invoiceAmount->fill($req->all());
        $invoiceAmount->save();
        $res['status'] = 'success';
        $res['msg'] = 'Amount added successfully.';
        return response()->json($res);
    }

    public function create_disputes(Request $req){
        $disputes = new Disputes();
        $disputes->fill($req->all());
        $disputes->save();
        $res['status'] = 'success';
        $res['msg'] = 'Disputes added successfully.';
        return response()->json($res);
    }

    public function add_user_bank_detail(Request $req){
        $bankDetail['bank_name'] = $req->bank_name;
        $bankDetail['account_no'] = $req->bank_account_no;
        $bankDetail['short_code'] = $req->bank_short_code;
        $bankDetail['bank_address'] = $req->bank_address;
        $bankDetail['bank_city'] = $req->bank_city;
        $bankDetail['bank_state'] = $req->bank_state;
        $bankDetail['bank_zip'] = $req->bank_zip;
        $bankDetail['bank_country'] = $req->bank_country;
        $bankDetail['bank_country_code'] = $req->bank_country_code;
        $bankDetail['bank_phone_no'] = $req->bank_phone_no;
        $userId = $req->user_id;
        $findUser = AppUsers::where('id',$userId)->first();

        $findUser->bank_detail = json_encode($bankDetail);
        $findUser->save();
        $res['status'] = 'success';
        $res['msg'] = 'Bank detail updated successfully.';
        $res['user'] = $findUser;
        return response()->json($res);
    }

    public function change_user_password(Request $req){
        $previousPassword = $req->previous_password;
        $userId = $req->user_id;
        $user = AppUsers::where('id', $userId)->first();
        if($user->password != md5($previousPassword)){
            $res['status'] = 'error';
            $res['msg'] = 'Wrong Current password';
            return response()->json($res);
        }
        $user->password = md5($req->new_password);
        $user->save();
        $res['status'] = 'success';
        $res['msg'] = 'Password updated';
        return response()->json($res);
    }

    public function update_user_profile(Request $req){
        $userId = $req->user_id;
        $user = AppUsers::where('id', $userId)->first();
        $user->f_name = $req->f_name;
        $user->l_name = $req->l_name;
        $user->phone = $req->phone;
        $user->address = $req->address;
        $user->city = $req->city;
        $user->zip = $req->zip;
        $user->state = $req->state;
        $user->country = $req->country;
        $user->country_code = $req->country_code;
        $user->save();
        $res['status'] = 'success';
        $res['msg'] = 'Profile updated';
        $res['user'] = $user;
        return response()->json($res);
    }

    public function uploadImage(Request $req){

        return response()->json($req);
    }

    public function savePdf(){
        echo asset('storage/pdf/d35118ab-5c6c-4559-8705-7923ee7c0e15.pdf');
    }
    public function getInvoicePdf($id){
        $invoice = Invoice::where('id', $id)->first();
        $gsd = json_decode($invoice->invoice_gs_detail);
        if($invoice->invoice_type == 'user') {
            $userDetail = AppUsers::where('id', $invoice->app_user_id)->first();
            $invoiceUser = $userDetail;
            $invoiceUser->full_name = $invoiceUser->f_name.' '.$invoiceUser->l_name;
        }else{
            $invoiceUser = json_decode($invoice->guest_user_detail);
        }
        $bankDetail = DB::table('bank_account')->first();
        $invoiceAmount = InvoiceAmount::where('invoice_id', $invoice->id)->get();
        $html =  view('pdf_view_two')->with('invoice', $invoice)->with('user', $invoiceUser)
            ->with('bankDetail', $bankDetail)->with('gsd', $gsd)->with('invoiceAmount', $invoiceAmount)->render();

//        $pdf = App::make('dompdf.wrapper');
        $pdf = PDF::loadHTML($html);
        $name = Str::uuid();
        $name = 'pdf/'.$name.'.pdf';
        Storage::disk('public')->put($name, $pdf->output());
//        Storage::put('public/'.$name, $pdf->output());
        $invoice->invoice_pdf = $name;
        $invoice->save();
        return $name;
    }

    public function saveContactUs(Request $req){
        $contactUs = new ContactUS();
        $contactUs->fill($req->all());
        $contactUs->save();
        $res['status'] = 'success';
        $res['msg'] = 'Data save successfully';
        return response()->json($res);
    }

    public function saveInvoiceDispute(Request $req){
        $invoice = Invoice::where('id', $req->invoice_id)->first();
        $invoice->invoice_status = 'dispute';
        $dispute['title'] = $req->title;
        $dispute['detail'] = $req->detail;
        $invoice->dispute_detail = json_encode($dispute);
        $invoice->save();
        $res['status'] = 'success';
        $res['msg'] = 'Data save successfully';
        return response()->json($res);

    }

    public function confirmInvoice(Request $req){
        $invoice = Invoice::where('id', $req->invoice_id)->first();
        $invoice->invoice_status = 'complete';
        $invoice->save();
        $res['status'] = 'success';
        $res['msg'] = 'Data save successfully';
        return response()->json($res);
    }


}
