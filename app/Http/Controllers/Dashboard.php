<?php

namespace App\Http\Controllers;

use App\Models\AppUsers;
use App\Models\ContactUS;
use App\Models\Disputes;
use Illuminate\Http\Request;

class Dashboard extends Controller
{
    function Dashboard(Type $var = null)
    {
        $appUser = AppUsers::count();
        $pendingInvoice = \App\Models\Invoice::where('invoice_status', 'pending')->count();
        $disputes = Disputes::where('status', 'pending')->count();

        return view('admin.dashboard')->with('appUsers', $appUser)->with('invoices', $pendingInvoice)
            ->with('disputes', $disputes);
    }


    public function contactUs(){
        $contactUs = ContactUS::all();
        return view('admin.contactus.index')->with('contactUs', $contactUs);
    }

    public function contactUsDelete($id){
        ContactUS::where('id', $id)->delete();
        return redirect('/contact_us');
    }

}
