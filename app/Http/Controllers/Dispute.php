<?php

namespace App\Http\Controllers;

use App\Http\Traits\LogsTrait;
use App\Models\AppUsers;
use App\Models\Disputes as DisputesModel;
use App\Models\UserModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;


class Dispute extends Controller
{
    use LogsTrait;
    public function allDispute(){
        $pending = DisputesModel::where('status', 'pending')->get();
        $process = DisputesModel::where('status', '!=', 'pending')->where('status', '!=', 'complete')->get();
        $complete = DisputesModel::where('status', 'complete')->get();
        return view('admin.dispute.index')->with('pending', $pending)->with('process', $process)
            ->with('complete', $complete);
    }

      public function ViewDispute($id){
        $dispute = DisputesModel::where('id', $id)->first();
        $user = AppUsers::where('id', $dispute->user_id)->first();
          $editedUser = null;
        if(isset($dispute->edited_by)){
            $editedUser = UserModel::where('id', $dispute->edited_by)->first();
        }
        return view('admin.dispute.dispute_view')->with('dispute', $dispute)->with('user', $user)
            ->with('editUser', $editedUser);
    }

    public function change_status($id, $status){
        $dispute = DisputesModel::where('id', $id)->first();
        $dispute->status = $status;
        $dispute->save();
        $this->add_log('update', 'Update dispute status', 'disputes', $id);
        return redirect('/dispute/');
    }

    public function delete_dispute($id){
        DisputesModel::where('id', $id)->delete();
        $this->add_log('delete', 'Dispute Deleted', 'disputes', $id);
        return redirect('/dispute/');
    }

    public function add_notes(Request $req){
        $id = $req->id;
        $notes = $req->notes;
        $dispute = DisputesModel::where('id', $id)->first();
        $dispute->notes = $notes;
        $user = Session::get('user');
        $dispute->edited_by = $user->id;
        $dispute->edit_time = (date('Y-m-d H:i:s'));
        $this->add_log('update', 'add notes to dispute ('.$notes.')', 'disputes', $id);
        $dispute->save();
        return redirect('dispute/dispute_detail/'.$id);
    }



}
