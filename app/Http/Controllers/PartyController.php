<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\PartyModel;

class PartyController extends Controller
{
    public function ViewParty(){
        return view('admin.party.index');
    }
    public function AddParty(Request $request){
        $postedData = $request->all();
        if(count($postedData) >= 1){
            $store = new PartyModel();
            $store->fill($postedData);
            $store->save();
            return back()->with('success','Data has been Store Successfully');
        }
        return view('admin.party.add');
    }
}
