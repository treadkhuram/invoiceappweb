<?php

namespace App\Http\Controllers;

use App\Http\Traits\LogsTrait;
use Illuminate\Http\Request;
use App\Models\UserModel;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class User extends Controller
{
    use LogsTrait;

    function Login( Request $request)
    {

            $store =UserModel::where('email',$request->email)->where('password',md5($request->password))->first();
            if(isset($store))
            {
                Session::put('user', $store);
                Session::put('active', 'authorize_user');
                 $auth = Session::all();
                 if($auth['active']=='authorize_user'){
                    return redirect('dashboard')->with('success','You have been successfully Logged In');
                 }
            }
            else
            {
                return view('admin.login');
            }

           // return back()->with('success','Data has been Store Successfully');


    }
    function login_user(Request $request){
        $store =UserModel::where('email',$request->email)->where('password',md5($request->password))->first();
        if(isset($store))
        {
            Session::put('user', $store);
            Session::put('active', 'authorize_user');
            $auth = Session::all();
            if($auth['active']=='authorize_user'){
                return redirect('dashboard')->with('success','You have been successfully Logged In');
            }
        }
        else
        {
            return redirect('/');
        }

    }

    function logout(){
        Session::flush();
        return redirect('/');
    }

    function update_profile(Request $req){

        if(isset($req->id)) {
            $user = UserModel::where('id', $req->id)->first();
//            $store = UserModel::where('id', $id)->update(['name' => $request->name, 'phone' => $request->phone,
//                'email' => $request->email, 'password' => $request->password, 'address' => $request->address]);
            $user->name = $req->name;
            $user->phone = $req->phone;
            $user->address = $req->address;

            if (isset($req->password))
                $user->password = md5($req->password);
            $user->save();
            Session::put('user', $user);
            $user = Session::get('user');
            $this->add_log('update', 'update user profile', 'user', $req->id);
            return view('admin.update_profile')->with('user', $user)->with('success', 'Data updated successfully.');
        }else{
            $user = Session::get('user');
            return view('admin.update_profile')->with('user', $user);
        }
    }

    public function ViewUser(){

        $userData['data'] = UserModel::all();
        return view('admin.user.index')->with('userData',$userData);
    }
    public function AddUser(Request $request){
        $postedData = $request->all();
        if(count($postedData) >= 1){
            $store = new UserModel();
            $store->fill($postedData);
            $store->password = md5($postedData['password']);
            $store->save();
            $this->add_log('add', 'Add user', 'user', $store->id);
            return back()->with('success','Data has been Store Successfully');
        }
        return view('admin.user.add');
    }

    public function DeleteUser($id)
    {
        $delete=new UserModel();
        $delete->deleteData($id);
        $this->add_log('delete', 'delete user', 'user', $id);
        return back()->with('success','Data has been Deleted Successfully');
    }

    public function EditUser($id)
    {
        $EditData=UserModel::where('id',$id)->get();
        return view('admin.user.edit')->with('EditData',$EditData);
    }

    public function SaveEditUser(Request $request)
    {

        $id=$request->id;

        $store = UserModel::where('id',$id)->update(['name'=>$request->name,'phone'=>$request->phone,'email'=>$request->email,'address'=>$request->address]);
        $this->add_log('update', 'uodate user', 'user', $id);
        $EditData=UserModel::where('id',$id)->get();
        return back()->with('success','Data has been Updated Successfully','EditData',$EditData);

    }

    public function saveEditRights(Request $request)
    {

     if($request->viewuser!='1') {
        $user['view']='0';
     }
     else {
        $user['view']=$request->viewuser;
     }
     if($request->edituser!='1') {
        $user['edit']='0';
     }
     else {
        $user['edit']=$request->edituser;
     }
     $userRight['user'] = $user;

     if($request->viewappuser!='1') {
        $appUser['view']='0';
     }
     else {
        $appUser['view']=$request->viewappuser;
     }
     if($request->editappuser!='1') {
        $appUser['edit']='0';
     }
     else {
        $appUser['edit']=$request->editappuser;
     }
     $userRight['app_user'] = $appUser;

     if($request->viewinvoice!='1') {
        $invoice['view']='0';

     }
     else {
        $invoice['view']=$request->viewinvoice;
     }
     if($request->editinvoice!='1') {
        $invoice['edit']='0';
     }
     else {
        $invoice['edit']=$request->editinvoice;
     }
     $userRight['invoice'] = $invoice;

        if($request->viewinvoice_amount!='1') {
            $invoice_amount['view']='0';

        }
        else {
            $invoice_amount['view']=$request->viewinvoice_amount;
        }
        if($request->editinvoice_amount!='1') {
            $invoice_amount['edit']='0';
        }
        else {
            $invoice_amount['edit']=$request->editinvoice_amount;
        }
        $userRight['invoice_amount'] = $invoice_amount;

     if($request->viewdispute!='1') {
         $dispute['view']='0';
     }
     else {
        $dispute['view']=$request->viewdispute;
     }
     if($request->editdispute!='1') {
        $dispute['edit']='0';
     }
     else {
        $dispute['edit']=$request->editdispute;
     }
     $userRight['dispute'] = $dispute;

        if($request->viewContactUs!='1') {
            $contactUs['view']='0';
        }
        else {
            $contactUs['view']=$request->viewContactUs;
        }
        if($request->editContactUs!='1') {
            $contactUs['edit']='0';
        }
        else {
            $contactUs['edit']=$request->editContactUs;
        }
        $userRight['contactus'] = $contactUs;

        $data=json_encode($userRight);
       //print_r($data);die;
        $id=$request->id;

        $store = UserModel::where('id',$id)->update(['user_rights'=>$userRight]);

        $EditData=UserModel::where('id',$id)->get();
        $this->add_log('add', 'Add user rights', 'user', $id);
        return back()->with('success','User Rights has been Updated Successfully','EditData',$EditData);

    }


    public function AssignRight($id){

        $EditData=UserModel::where('id',$id)->get();
        return view('admin.user.right')->with('EditData',$EditData);
    }

    public function bankDetail(Request $req){
        $bankAccount = DB::table('bank_account')->first();
        if(isset($req->bank_name)){
            $bankAccount->bank_name = $req->bank_name;
            $bankAccount->account_no = $req->account_no;
            $bankAccount->short_code = $req->short_code;
            $bankAccount->phone = $req->phone;
            $bankAccount->city = $req->city;
            $bankAccount->state = $req->state;
            $bankAccount->zip = $req->zip;
            $bankAccount->country = $req->country;
            DB::table('bank_account')->where('id',1)
                ->update(['bank_name'=>$req->bank_name, 'account_no'=>$req->account_no,'short_code'=>$req->short_code,
                    'phone'=>$req->phone, 'city'=>$req->city, 'state'=>$req->state, 'zip'=>$req->zip,
                    'country'=>$req->country]);
        }
        return view('admin.payment_method')->with('bank_account', $bankAccount);
    }
}
