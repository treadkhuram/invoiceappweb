<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class Role
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if(Session::get('active') == 'authorize_user') {
            $user = Session::get('user');
            if(isset($user->user_rights)){
                return $next($request);
            }
            return redirect('/')->with('error','Permission Denied!!! You do not have administrative access.');
        }
        return redirect('/')->with('error','Permission Denied!!! You do not have administrative access.');
    }
}
