<?php

namespace App\Models;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserModel extends Model
{
    use HasFactory;
    public $timestamps=false;
    protected $table = 'user';
    protected $fillable = ['name','email','phone','password','address', 'user_type'];

    public static function deleteData($id){
        DB::table('user')->where('id', '=', $id)->delete();
      }
}

