<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class StockModel extends Model
{
    use HasFactory;
    protected $table = 'stock';
    protected $fillable = ['name','quantity','size','sale_price','supplier_sale_price','material_cost','production_cost','packing_cost'];
}
