<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AppUsers extends Model
{
    use HasFactory;

    protected $table = 'app_users';

    public $timestamps = true;
    protected $attributes=['verified'=>0];
    protected $hidden = ['password', 'verified_token'];
    protected $fillable = ['f_name', 'l_name', 'phone', 'email', 'address', 'city', 'zip',
        'state', 'country', 'country_code', 'password', 'verified', 'verified_token', 'bank_detail'];

}
