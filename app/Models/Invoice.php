<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{
    use HasFactory;
    protected $table = 'invoice';
    public $timestamps = true;

//    protected $casts =[
//        'expiry_date' => 'date'
//    ];

    protected $fillable = ['invoice_no', 'name', 'images', 'expiry_date', 'bank_name', 'amount', 'currency', 'detail', 'invoice_for', 'invoice_gs_detail',
        'delivery_require', 'shipment_mode', 'delivery_days', 'shipment_cost', 'delivery_option',
        'end_contract', 'pay_service_charges', 'invoice_type','invoice_status',
        'guest_user_detail', 'guest_bank_detail', 'dispute_detail', 'invoice_password', 'app_user_id', 'accepted_by', 'accepted_date', 'invoice_pdf'];


    public function createBy(){
        return $this->belongsTo(AppUsers::class, 'app_user_id');
    }




}
