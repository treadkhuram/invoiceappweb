<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PartyModel extends Model
{
    protected $table = 'party';
    protected $fillable = ['name','mobile_no','address','detail','type'];
}
