<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class InvoiceAmount extends Model{
    use HasFactory;
    protected $table = 'invoice_amount';

    protected $fillable = ['transaction_name', 'bank_name', 'paid_amount', 'paid_date', 'reference_no',
        'verified', 'invoice_id'];

}
