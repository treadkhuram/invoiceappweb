<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class system_logs extends Model
{
    use HasFactory;
    protected $table = 'system_logs';

    protected $fillable = ['actions', 'action_detail', 'user_id', 'table', 'table_id'];

    public function user(){
        return $this->belongsTo(UserModel::class);
    }

}
