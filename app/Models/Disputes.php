<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Disputes extends Model{
    use HasFactory;
    protected $table = 'disputes';

    protected $fillable = ['invoice_id', 'email', 'title', 'description', 'status', 'notes', 'edited_by', 'edit_time', 'user_id'];
}
