<?php

namespace App\Providers;

use Illuminate\Support\Facades\Session;
use Illuminate\Support\ServiceProvider;

class RuleServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        view()->composer('*', function ($view){
            if(Session::get('active') == 'authorize_user'){
                $user = Session::get('user');
                $userRights = json_decode($user->user_rights);
                return $view->with('my_rights', $userRights)->with('login_user_type', $user->user_type);
            }

        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
