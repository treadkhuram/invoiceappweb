<?php
/**
 * Created by PhpStorm.
 * User: Omen
 * Date: 9/4/2021
 * Time: 12:07 PM
 */

namespace App;


class App_helper{
    public function parse_date($date){
        return date('d/m/Y', strtotime($date));
    }
}