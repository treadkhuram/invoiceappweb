<?php

use App\Http\Controllers\PartyController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\StockController;
use App\Http\Controllers\AppUser;
use App\Http\Controllers\Invoice;
use App\Http\Controllers\Dispute;
use App\Http\Controllers\User;
use App\Http\Controllers\Dashboard;
use App\Http\Controllers\MobileController;
use App\Http\Controllers\SystemLogs;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('admin.dashboard');
// });

Route::get('/',[User::class,'Login'])->name('login');
Route::post('/login_user',[User::class,'login_user'])->name('login_user');
Route::get('logout', [User::class, 'logout'])->name('logout');
Route::get('verify_user/{id}',[MobileController::class,'verify_user']);
Route::get('reset_password/{id}',[MobileController::class, 'reset_password']);
Route::post('update_password', [MobileController::class, 'update_password']);
Route::get('getInvoicePdf/{id}', [MobileController::class, 'getInvoicePdf']);
Route::get('getInvoicePdf2/{id}', [MobileController::class, 'getInvoicePdf2']);
Route::get('savePdf', [MobileController::class, 'savePdf']);
Route::group(['middleware' => 'role'], function (){

    Route::get('/dashboard',[Dashboard::class,'Dashboard'])->name('dashboard');
    Route::get('/contact_us', [Dashboard::class, 'contactUs'])->name('contactUs');
    Route::get('/delete_contact_us/{id}', [Dashboard::class, 'contactUsDelete'])->name('delete_contact_us');

    //stock routes
    Route::prefix('stock')->group(function () {
        Route::get('/',[StockController::class,'ViewStock'])->name('view.stock');
        Route::get('/add',[StockController::class,'AddStock'])->name('add.stock');
        Route::post('/add',[StockController::class,'AddStock'])->name('add.stock');
    });
    //party routes
    Route::prefix('party')->group(function () {
        Route::get('/',[PartyController::class,'ViewParty'])->name('view.party');
        Route::get('/add',[PartyController::class,'AddParty'])->name('add.party');
        Route::post('/add',[PartyController::class,'AddParty'])->name('add.party');
    });

    Route::prefix('appUser')->group(function () {
        Route::get('/',[AppUser::class,'ViewParty'])->name('view.appUser');
        Route::get('/delete_app_user/{id}',[AppUser::class,'delete_app_user'])->name('delete_app_user');
        Route::get('/view_detail/{id}',[AppUser::class, 'view_detail'])->name('view_detail');
        Route::post('/update_detail', [AppUser::class, 'update_detail'])->name('update_app_user_detail');
    });
    Route::prefix('invoice')->group(function () {
        Route::get('/',[Invoice::class,'allInvoices'])->name('view.invoice');
        Route::get('/viewInvoice/{id}',[Invoice::class,'ViewInvoice'])->name('view.invoicedetails');
        Route::get('/payment', [Invoice::class, 'payment'])->name('payment');
        Route::get('/approve_payment/{id}', [Invoice::class, 'approve_payment'])->name('approve_payment');
        Route::get('/delete_payment/{id}', [Invoice::class, 'delete_payment'])->name('delete_payment');
        Route::get('/delete_invoice/{id}', [Invoice::class, 'delete_invoice'])->name('delete_invoice');
        Route::get('/update_status/{id}/{status}',[Invoice::class, 'update_status'])->name('update_invoice_status');
        Route::get('/dispute_detail/{id}',[Invoice::class, 'invoice_dispute_detail'])->name('invoice_dispute_detail');
        Route::post('/add_dispute_notes',[Invoice::class, 'add_dispute_notes'])->name('add_dispute_notes');
    });

    Route::prefix('dispute')->group(function () {
        Route::get('/',[Dispute::class,'allDispute'])->name('view.dispute');
        Route::get('/change_status/{id}/{status}', [Dispute::class, 'change_status'])->name('change_status');
        Route::get('/delete_dispute/{id}', [Dispute::class, 'delete_dispute'])->name('delete_dispute');
        Route::get('/dispute_detail/{id}', [Dispute::class, 'ViewDispute'])->name('dispute_detail');
        Route::post('/add_notes',[Dispute::class, 'add_notes'])->name('add_notes');
    });

    Route::prefix('user')->group(function () {
        Route::get('/',[User::class,'ViewUser'])->name('view.user');
        Route::get('/add',[User::class,'AddUser'])->name('add.user');
        Route::post('/add',[User::class,'AddUser'])->name('add.user');
        Route::get('/edit/{id}',[User::class,'EditUser'])->name('edit.user');
      //  Route::get('/edit/{id}',[User::class,'EditUSer'])->name('save.edit');
        Route::post('/edit',[User::class,'SaveEditUser'])->name('save.edit');
        Route::get('/right/{id}',[User::class,'AssignRight'])->name('right.user');
        Route::post('/editRight',[User::class,'saveEditRights'])->name('save.edit.right');
        Route::get('/delete/{id}',[User::class,'DeleteUser'])->name('delete.user');
        Route::get('update_profile',[User::class, 'update_profile'])->name('update_profile');
        Route::post('update_profile',[User::class, 'update_profile'])->name('update_profile');
        Route::get('bank_account', [User::class, 'bankDetail'])->name('bank_detail');
        Route::post('bank_account', [User::class, 'bankDetail'])->name('bank_detail');
    });
    Route::prefix('logs')->group(function (){
        Route::get('/', [SystemLogs::class, 'index'])->name('system_logs');
        Route::get('delete_log/{id}',[SystemLogs::class, 'delete_log'])->name('delete_log');
    });
});
