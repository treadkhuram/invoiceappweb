<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\MobileController;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('check', [MobileController::class, 'check']);
Route::post('create_account', [MobileController::class, 'create_account']);
Route::post('login', [MobileController::class, 'login']);
Route::post('create_invoice', [MobileController::class, 'create_invoice']);
Route::get('get_invoice_no', [MobileController::class, 'getInvoiceNumber']);
Route::post('get_all_invoices', [MobileController::class, 'getAllInvoices']);
Route::post('search_invoice', [MobileController::class, 'searchInvoice']);
Route::get('send_mail', [MobileController::class, 'composeEmail']);
Route::post('accept_reject_invoice', [MobileController::class, 'accept_reject_invoice']);
Route::post('reset_password_request', [MobileController::class, 'resetPasswordRequest']);
Route::post('create_payment', [MobileController::class, 'create_payment']);
Route::post('create_disputes', [MobileController::class, 'create_disputes']);
Route::post('add_user_bank_detail', [MobileController::class, 'add_user_bank_detail']);
Route::post('change_user_password', [MobileController::class, 'change_user_password']);
Route::post('update_user_profile', [MobileController::class, 'update_user_profile']);
Route::post('upload_image', [MobileController::class, 'uploadImage']);
Route::get('get_invoice_pdf', [MobileController::class, 'getInvoicePdf']);
Route::post('save_contact_us', [MobileController::class, 'saveContactUs']);
Route::post('save_invoice_dispute', [MobileController::class, 'saveInvoiceDispute']);
Route::post('confirm_invoice', [MobileController::class, 'confirmInvoice']);

