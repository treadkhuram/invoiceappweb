@extends('admin.layout.interface')

@section('bread-cumbs')
    @include('admin.layout.breadcrumbs',
    ['breadcrumbs' =>
        ['title' => 'Stock',
         'items' => [
             ['name' => 'Home', 'url' => url("/"), 'active' => false],
             ['name' => 'Stock', 'url' => url("/"), 'active' => true],

         ]
        ]
    ])
@endsection

@section('content')

    <div class="row">
        <div class="col">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title" id="horz-layout-colored-controls">User Details</h4>
                </div>
                 <div class="table-responsive">
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>Full Name</th>
                                                    <th>Phone</th>
                                                    <th>Address</th>
                                                    <th>Email</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <th scope="row">1</th>
                                                    <td>Mark</td>
                                                    <td>+6955555</td>
                                                    <td>Vetinum</td>
                                                    <td>ellery@gmail.com</td>
                                                    <td>
                                                        <i class="ft-edit-2"></i>
                                                        <i class="ft-alert-circle">
                                                        <i class="ft-trash-2"></i>
                                                        </i>
                                                    </td>

                                                </tr>

                                                <tr>
                                                    <th scope="row">2</th>
                                                    <td>Spenser</td>
                                                    <td>+6998222</td>
                                                    <td>Vetinum</td>
                                                    <td>spener@gmail.com</td>
                                                    <td>
                                                        <i class="ft-edit-2"></i>
                                                        <i class="ft-alert-circle">
                                                        <i class="ft-trash-2"></i>
                                                        </i>
                                                    </td>

                                                </tr>
                                                <tr>
                                                    <th scope="row">3</th>
                                                    <td>goliza</td>
                                                    <td>+123654</td>
                                                    <td>Vetinum</td>
                                                    <td>goliza@gmail.com</td>
                                                    <td>
                                                        <i class="ft-edit-2"></i>
                                                        <i class="ft-alert-circle">
                                                        <i class="ft-trash-2"></i>
                                                        </i>
                                                    </td>

                                                </tr>
                                               
                                                
                                            </tbody>
                                        </table>
                                    </div>
            </div>
        </div>
    </div>
@endsection
