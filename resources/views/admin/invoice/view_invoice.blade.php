@extends('admin.layout.interface')

@section('bread-cumbs')
    @include('admin.layout.breadcrumbs',
    ['breadcrumbs' =>
        ['title' => 'Stock',
         'items' => [
             ['name' => 'Home', 'url' => url("/"), 'active' => false],
             ['name' => 'Stock', 'url' => url("/"), 'active' => true],

         ]
        ]
    ])
@endsection

@section('content')

    <div class="app-content">
        <div class="app-content">
            <div class="content-wrapper-before"></div>
            <div class="content-header row">
                <div class="content-header-left col-md-4 col-12 mb-2">
                    <h3 class="content-header-title">Invoice</h3>
                </div>
                <div class="content-header-right col-md-8 col-12">
                    <div class="breadcrumbs-top float-md-right">
                        <div class="breadcrumb-wrapper mr-1">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="index.html">Invoice</a>
                                </li>

                                <li class="breadcrumb-item active">Invoice detail
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
            <div class="content-body">
                <section class="card">
                    <div id="invoice-template" class="card-body">
                        <!-- Invoice Company Details -->
                        <div id="invoice-company-details" class="row">
                            <div class="col-md-6 col-sm-12 text-left text-md-left">
                                <h2>INVOICE</h2>
                                <p>{{$invoice->invoice_no}}</p>
                                <p>Invoice Name: {{$invoice->name}}</p>
                                <ul class="px-0 list-unstyled">
                                    <li>Invoice status:
                                        <div class="btn-group mr-1 mb-1">
                                            <button class="btn btn-danger btn-sm dropdown-toggle"
                                                    type="button" id="dropdownMenuButton6"
                                                    data-toggle="dropdown" aria-haspopup="true"
                                                    aria-expanded="false">
                                                {{$invoice->invoice_status}}
                                            </button>
                                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                                <a class="dropdown-item" href="{{route('update_invoice_status',[$invoice->id, 'pending'])}}" >Pending</a>
                                                <a class="dropdown-item" href="{{route('update_invoice_status',[$invoice->id, 'in-process'])}}">in-process</a>
                                                <a class="dropdown-item" href="{{route('update_invoice_status',[$invoice->id, 'complete'])}}">Complete</a>
                                                <a class="dropdown-item" href="{{route('update_invoice_status',[$invoice->id, 'canceled'])}}">Cancel</a>
                                            </div>
                                        </div>
                                        <i class="ft-edit-2 text-danger"></i>
                                        </li>
                                    <li>Create date: {{date('d-m-Y', strtotime($invoice->created_at))}} </li>
                                    <li>Due Date: {{date('d-m-Y', strtotime($invoice->expiry_date))}} </li>
                                </ul>
                            </div>
                            <div class="col-md-6 col-sm-12 text-center text-md-right">
                                <h2>Bill To</h2>
                                <ul class="px-0 list-unstyled">
                                    <li class="text-bold-700">{{$user->full_name}}</li>
                                    <li>Phone: {{$user->phone}}</li>
                                    <li>Address: {{$user->address}}</li>
                                    <li>City: {{$user->city}}</li>
                                    <li>State: {{$user->state}}</li>
                                    <li>Zip: {{$user->zip}}</li>
                                    <li>Country: {{$user->country}}</li>

                                </ul>

                            </div>
                        </div>

                        <h3>Description</h3>

                        <p>{{$invoice->detail}}</p>

                        <div class="card pull-up border-top-info border-top-3 rounded-0"></div>

                        <div class="row">
                            @if($invoice->invoice_for == '0')
                                <div class="col-md-6 col-sm-12 text-center text-md-left">
                                    <p class="lead">Invoice for Goods:</p>
                                    <div class="row">
                                        <div class="col-md-8">
                                            <table class="table table-borderless table-sm">
                                                <tbody>
                                                    <tr>
                                                        <td>Brand name:</td>
                                                        <td class="text-right">{{$gsd->brand_name}}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Model No:</td>
                                                        <td class="text-right">{{$gsd->model_no}}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Serial No:</td>
                                                        <td class="text-right">{{$gsd->serial_no}}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Manufacture year:</td>
                                                        <td class="text-right">{{$gsd->year_of_manufacture}}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Product condition</td>
                                                        <td class="text-right">{{$gsd->product_condition}}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Extra links</td>
                                                        <td class="text-right">{{$gsd->extra_link}}</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                            <?php $myImages = json_decode($invoice->images) ?>
                                            @if(count($myImages)>0)
                                            <div>
                                                <label>Images</label>
                                                <div class="row">
                                                    @foreach($myImages as $img)
                                                        <img height="100" src="{{asset('storage/'.$img)}}"/>
                                                    @endforeach
                                                </div>
                                            </div>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            @endif
                            @if($invoice->invoice_for == '1')
                                <div class="col-md-6 col-sm-12 text-center text-md-left">
                                    <p class="lead">Invoice for Services:</p>
                                    <div class="row">
                                        <div class="col-md-8">
                                            <table class="table table-borderless table-sm">
                                                <tbody>
                                                <tr>
                                                    <td>Service title:</td>
                                                    <td class="text-right">{{$gsd->service_title}}</td>
                                                </tr>
                                                <tr>
                                                    <td>Service description</td>
                                                    <td class="text-right">{{$gsd->service_description}}</td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            @endif
                                @if($invoice->delivery_require == 'yes')
                                    <div class="col-md-6 col-sm-12 text-center text-md-left">
                                        <p class="lead">Delivery Options:</p>
                                        <div class="row">
                                            <div class="col-md-8">
                                                <table class="table table-borderless table-sm">
                                                    <tbody>
                                                    <tr>
                                                        <td>Delivery Require:</td>
                                                        <td class="text-right">Yes</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Delivery Days</td>
                                                        <td class="text-right">{{$invoice->delivery_days}}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Mode of shipment</td>
                                                        <td class="text-right">{{$invoice->shipment_mode}}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Shipment cost:</td>
                                                        <td class="text-right">{{$invoice->shipment_cost}}</td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                @endif
                                @if($invoice->delivery_require == 'no')
                                    <div class="col-md-6 col-sm-12 text-center text-md-left">
                                        <p class="lead">Delivery Options:</p>
                                        <div class="row">
                                            <div class="col-md-8">
                                                <table class="table table-borderless table-sm">
                                                    <tbody>
                                                    <tr>
                                                        <td>Delivery Require:</td>
                                                        <td class="text-right">No</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Contract end in day's</td>
                                                        <td class="text-right">{{$invoice->end_contract}}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Who pay service charges</td>
                                                        <td class="text-right">{{$invoice->pay_service_charges}}</td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                @endif
                        </div>

                        <div class="row">
                            <div class="col-md-6 col-sm-12 text-center text-md-left">

                                <p class="lead">{{$invoice->invoice_type == 'user' ? 'Bank Detail' : 'E-transfer' }}</p>
                                <div class="row">
                                    <div class="col-md-8">
                                        <table class="table table-borderless table-sm">
                                            @if($invoice->invoice_type == 'user')
                                               <tbody>
                                                    <tr>
                                                        <td>Bank name:</td>
                                                        <td class="text-right">{{$bankDetail->bank_name}}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Bank phone number:</td>
                                                        <td class="text-right">{{$bankDetail->bank_phone_no}}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Account no:</td>
                                                        <td class="text-right">{{$bankDetail->account_no}}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Code:</td>
                                                        <td class="text-right">{{$bankDetail->short_code}}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Bank address:</td>
                                                        <td class="text-right">{{$bankDetail->bank_address}}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>City:</td>
                                                        <td class="text-right">{{$bankDetail->bank_city}}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Bank State:</td>
                                                        <td class="text-right">{{$bankDetail->bank_state}}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Bank Zip:</td>
                                                        <td class="text-right">{{$bankDetail->bank_zip}}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Bank country:</td>
                                                        <td class="text-right">{{$bankDetail->bank_country}}</td>
                                                    </tr>
                                               </tbody>
                                            @else
                                            <tbody>
                                                <tr>
                                                    <td>email</td>
                                                    <td class="text-right">{{isset($bankDetail->e_email) ? $bankDetail->e_email : ''}}</td>
                                                </tr>
                                                <tr>
                                                    <td>Phone #</td>
                                                    <td class="text-right">{{isset($bankDetail->e_phone) ? $bankDetail->e_phone : ''}}</td>
                                                </tr>
                                            </tbody>
                                            @endif
                                        </table>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6 col-sm-12 text-center text-md-left">

                                <p class="lead">Amount Received:</p>
                                <div class="row">
                                    <div class="col-md-8">
                                        <table class="table table-borderless table-sm">
                                            <thead>
                                            <tr>
                                                <th>T. Name</th>
                                                <th>Amount</th>
                                                <th>date</th>
                                                <th>verified</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php $receivedAmount = 0; ?>
                                            @foreach($invoiceAmount as $ia)
                                                <?php
                                                        if($ia->verified == 1){
                                                            $receivedAmount = $receivedAmount + $ia->paid_amount;
                                                        }
                                                 ?>
                                                <tr>
                                                    <td class="text-right">{{$ia->transaction_name}}</td>
                                                    <td class="text-right">{{$ia->paid_amount}}</td>
                                                    <td class="text-right">{{date('d/m/Y', strtotime($ia->bank_name))}}</td>
                                                    <td class="text-right">{{$ia->verified == 0 ? 'No':'Yes'}}</td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="card pull-up border-top-info border-top-3 rounded-0"></div>
                        <div class="col-md-12 col-sm-12 text-center text-md-right">
                            <ul class="px-0 list-unstyled">
                                <li>Total Amount: {{$invoice->amount + $invoice->shipment_cost}}</li>
                                <li>Receive verified amount: {{$receivedAmount}}</li>
                                <li>Balance: {{$invoice->amount + $invoice->shipment_cost - $receivedAmount}}</li>

                            </ul>

                        </div>
                    </div>

                </section>
            </div>
            </section>
        </div>
    </div>
    </div>

@endsection
