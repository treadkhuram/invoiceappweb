@extends('admin.layout.interface')

@section('bread-cumbs')
    @include('admin.layout.breadcrumbs',
    ['breadcrumbs' =>
        ['title' => 'Stock',
         'items' => [
             ['name' => 'Home', 'url' => url("/"), 'active' => false],
             ['name' => 'Stock', 'url' => url("/"), 'active' => true],

         ]
        ]
    ])
@endsection

@section('content')

    <div class="row">
        <div class="col">

            <div class="card" style="height: 334px;">
                <div class="card-header">
                    <h4 class="card-title">Invoice Payment's</h4>
                </div>
                <div class="card-content">
                    <div class="card-body">
                        <ul class="nav nav-tabs">
                            <li class="nav-item">
                                <a class="nav-link active" id="base-tab1" data-toggle="tab" aria-controls="tab1" href="#tab1" aria-expanded="true">Pending Payment</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="base-tab2" data-toggle="tab" aria-controls="tab2" href="#tab2" aria-expanded="false">Verified Payment</a>
                            </li>
                        </ul>
                        <div class="tab-content px-1 pt-1">
                            <div role="tabpanel" class="tab-pane active" id="tab1" aria-expanded="true" aria-labelledby="base-tab1">
                                <div class="table-responsive">
                                    <table class="table">
                                        <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Transaction Name</th>
                                            <th>Bank Name</th>
                                            <th>Amount</th>
                                            <th>Date</th>
                                            <th>Reference No</th>
                                            <th>Action</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($pending as $key=>$pen)
                                            <tr>
                                                <td>{{$key+1}}</td>
                                                <td>{{$pen->transaction_name}}</td>
                                                <td>{{$pen->bank_name}}</td>
                                                <td>{{$pen->paid_amount}}</td>
                                                <td>{{date('d/m/Y', strtotime($pen->paid_date))}}</td>
                                                <td>{{$pen->reference_no}}</td>
                                                <td>
                                                    <div class="btn-group mr-1 mb-1">
                                                        <button class="btn btn-danger btn-sm dropdown-toggle" type="button" id="dropdownMenuButton6" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                            Action
                                                        </button>
                                                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                                            @if($my_rights->invoice_amount->edit)
                                                                <a class="dropdown-item" href="{{route('approve_payment', $pen->id)}}">Approve</a>
                                                                <a class="dropdown-item" href="{{route('delete_payment', $pen->id)}}">Delete</a>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>

                            </div>
                            <div class="tab-pane" id="tab2" aria-labelledby="base-tab2">
                                <div class="table-responsive">
                                    <table class="table">
                                        <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Transaction Name</th>
                                            <th>Bank Name</th>
                                            <th>Amount</th>
                                            <th>Date</th>
                                            <th>Reference No</th>
                                            <th>Action</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($complete as $key=>$pen)
                                            <tr>
                                                <td>{{$key+1}}</td>
                                                <td>{{$pen->transaction_name}}</td>
                                                <td>{{$pen->bank_name}}</td>
                                                <td>{{$pen->paid_amount}}</td>
                                                <td>{{date('d/m/Y', strtotime($pen->paid_date))}}</td>
                                                <td>{{$pen->reference_no}}</td>
                                                <td>
                                                    <div class="btn-group mr-1 mb-1">
                                                        <button class="btn btn-danger btn-sm dropdown-toggle" type="button" id="dropdownMenuButton6" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                            Action
                                                        </button>
                                                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                                            @if($my_rights->invoice_amount->edit)
                                                                <a class="dropdown-item" href="{{route('delete_payment', $pen->id)}}">Delete</a>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


        </div>
    </div>
@endsection
