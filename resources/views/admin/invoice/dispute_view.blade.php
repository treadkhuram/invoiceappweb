@extends('admin.layout.interface')

@section('bread-cumbs')
    @include('admin.layout.breadcrumbs',
    ['breadcrumbs' =>
        ['title' => 'Stock',
         'items' => [
             ['name' => 'Home', 'url' => url("/"), 'active' => false],
             ['name' => 'Stock', 'url' => url("/"), 'active' => true],

         ]
        ]
    ])
@endsection

@section('content')

    <div class="app-content">
        <div class="app-content">
            <div class="content-wrapper-before"></div>
            <div class="content-header row">
                <div class="content-header-left col-md-4 col-12 mb-2">
                    <h3 class="content-header-title">Invoice Dispute Detail</h3>
                </div>

            </div>
            <div class="content-body">
                <section class="card">
                    <div id="invoice-template" class="card-body">
                        <!-- Invoice Company Details -->
                        <div id="invoice-company-details" class="row">
                            <div class="col-md-6 col-sm-12 text-left text-md-left">
                                <!--  <img src="app-assets/images/logo/logo-80x80.png" alt="company logo" class="mb-2" width="70"> -->
                                <ul class="px-0 list-unstyled">
                                    <li><h1>Invoice# {{$invoice->invoice_no}}</h1></li>
                                </ul>

                            </div>

                        </div>
                        <?php $dispute = json_decode($invoice->dispute_detail) ?>
                        <!-- Invoice Customer Details -->
                        <div id="invoice-customer-details" class="row pt-2">
                            <div class="col-md-6 col-sm-12">
                                <p class="text-muted"><b> Title: {{$dispute->title}}</b></p>
                            </div>

                        </div>
                        <!-- Invoice Items Details -->
                        <div id="invoice-items-details" class="pt-2">


                        </div>
                        <h3>Dispute Detail</h3>

                        <p>{{$dispute->detail}}</p>


                        <div class="mt-5">
                            <form action="{{url('/invoice/add_dispute_notes')}}" method="post">
                                <input type="hidden" name="id" value="{{$invoice->id}}">
                                <textarea class="col-md-12" name="notes" required rows="5">{{isset($dispute->notes) ? $dispute->notes : ''}}</textarea>
                                <button class="btn btn-primary float-right">Save</button>
                            </form>
                        </div>


                    </div>


                </section>
            </div>
        </div>
    </div>

@endsection
