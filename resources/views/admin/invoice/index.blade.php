@extends('admin.layout.interface')

@section('bread-cumbs')
    @include('admin.layout.breadcrumbs',
    ['breadcrumbs' =>
        ['title' => 'Stock',
         'items' => [
             ['name' => 'Home', 'url' => url("/"), 'active' => false],
             ['name' => 'Stock', 'url' => url("/"), 'active' => true],

         ]
        ]
    ])
@endsection

@section('content')

    <div class="row">
        <div class="col">

            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">Invoice's</h4>
                </div>
                <div class="card-content">
                    <div class="card-body">
                        <ul class="nav nav-tabs">
                            <li class="nav-item">
                                <a class="nav-link active" id="base-tab1" data-toggle="tab" aria-controls="tab1"
                                   href="#tab1" aria-expanded="true">New</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="base-tab2" data-toggle="tab" aria-controls="tab2" href="#tab2"
                                   aria-expanded="false">In-process</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="base-tab3" data-toggle="tab" aria-controls="tab3" href="#tab3"
                                   aria-expanded="false">Completed</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link text-danger" id="base-tab4" data-toggle="tab" aria-controls="tab4" href="#tab4"
                                   aria-expanded="false">Dispute</a>
                            </li>
                        </ul>
                        <div class="tab-content px-1 pt-1">
                            <div role="tabpanel" class="tab-pane active" id="tab1" aria-expanded="true"
                                 aria-labelledby="base-tab1">
                                <div class="table-responsive">
                                    <table class="table">
                                        <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Contract #</th>
                                            <th>Contract Name</th>
                                            <th>Expiry Date</th>
                                            <th>Amount</th>
                                            <th>Action</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($pending as $key=>$pen)
                                            <tr>
                                                <th scope="row">{{$key+1}}</th>
                                                <td>{{$pen->invoice_no}}</td>
                                                <td>{{$pen->name}}</td>
                                                <td>{{date('d/m/Y', strtotime($pen->expiry_date))}}</td>
                                                <td>{{$pen->amount}}</td>

                                                <td>

                                                    <div class="btn-group mr-1 mb-1">
                                                        <button class="btn btn-danger btn-sm dropdown-toggle"
                                                                type="button" id="dropdownMenuButton6"
                                                                data-toggle="dropdown" aria-haspopup="true"
                                                                aria-expanded="false">
                                                            Action
                                                        </button>
                                                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                                            <a class="dropdown-item"
                                                               href="{{route('view.invoicedetails',$pen->id)}}">Detail</a>
                                                            @if($my_rights->invoice->edit)
                                                                <a class="dropdown-item" onclick="confirmDelete('{{route('delete_invoice', $pen->id)}}')">Delete</a>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </td>

                                            </tr>
                                        @endforeach


                                        </tbody>
                                    </table>
                                </div>

                            </div>
                            <div class="tab-pane" id="tab2" aria-labelledby="base-tab2">
                                <div class="table-responsive">
                                    <table class="table">
                                        <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Contract #</th>
                                            <th>Contract Name</th>
                                            <th>Expiry Date</th>
                                            <th>Amount</th>
                                            <th>status</th>
                                            <th>Action</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($processing as $key=>$pen)
                                            <tr>
                                                <th scope="row">{{$key+1}}</th>
                                                <td>{{$pen->invoice_no}}</td>
                                                <td>{{$pen->name}}</td>
                                                <td>{{date('d/m/Y', strtotime($pen->expiry_date))}}</td>
                                                <td>{{$pen->amount}}</td>
                                                <td>{{$pen->invoice_status}}</td>

                                                <td>

                                                    <div class="btn-group mr-1 mb-1">
                                                        <button class="btn btn-danger btn-sm dropdown-toggle"
                                                                type="button" id="dropdownMenuButton6"
                                                                data-toggle="dropdown" aria-haspopup="true"
                                                                aria-expanded="false">
                                                            Action
                                                        </button>
                                                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                                            <a class="dropdown-item"
                                                               href="{{route('view.invoicedetails',$pen->id)}}">Detail</a>
                                                            @if($my_rights->invoice->edit)
                                                                <a class="dropdown-item" onclick="confirmDelete('{{route('delete_invoice', $pen->id)}}')">Delete</a>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </td>

                                            </tr>
                                        @endforeach


                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="tab-pane" id="tab3" aria-labelledby="base-tab3">
                                <div class="table-responsive">
                                    <table class="table">
                                        <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Contract #</th>
                                            <th>Contract Name</th>
                                            <th>Expiry Date</th>
                                            <th>Amount</th>
                                            <th>Action</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($complete as $key=>$pen)
                                            <tr>
                                                <th scope="row">{{$key+1}}</th>
                                                <td>{{$pen->invoice_no}}</td>
                                                <td>{{$pen->name}}</td>
                                                <td>{{date('d/m/Y', strtotime($pen->expiry_date))}}</td>
                                                <td>{{$pen->amount}}</td>

                                                <td>

                                                    <div class="btn-group mr-1 mb-1">
                                                        <button class="btn btn-danger btn-sm dropdown-toggle"
                                                                type="button" id="dropdownMenuButton6"
                                                                data-toggle="dropdown" aria-haspopup="true"
                                                                aria-expanded="false">
                                                            Action
                                                        </button>
                                                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                                            <a class="dropdown-item"
                                                               href="{{route('view.invoicedetails',$pen->id)}}">Detail</a>
                                                            @if($my_rights->invoice->edit)
                                                                <a class="dropdown-item" onclick="confirmDelete('{{route('delete_invoice', $pen->id)}}')">Delete</a>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </td>

                                            </tr>
                                        @endforeach


                                        </tbody>
                                    </table>
                                </div>
                            </div>

                            <div role="tabpanel" class="tab-pane" id="tab4" aria-expanded="true"
                                 aria-labelledby="base-tab4">
                                <div class="table-responsive">
                                    <table class="table">
                                        <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Contract #</th>
                                            <th>Contract Name</th>
                                            <th>Expiry Date</th>
                                            <th>Amount</th>
                                            <th>Action</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($disputes as $key=>$des)
                                            <tr>
                                                <th scope="row">{{$key+1}}</th>
                                                <td>{{$des->invoice_no}}</td>
                                                <td>{{$des->name}}</td>
                                                <td>{{date('d/m/Y', strtotime($des->expiry_date))}}</td>
                                                <td>{{$des->amount}}</td>

                                                <td>

                                                    <div class="btn-group mr-1 mb-1">
                                                        <button class="btn btn-danger btn-sm dropdown-toggle"
                                                                type="button" id="dropdownMenuButton6"
                                                                data-toggle="dropdown" aria-haspopup="true"
                                                                aria-expanded="false">
                                                            Action
                                                        </button>
                                                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                                            <a class="dropdown-item" href="{{route('view.invoicedetails',$des->id)}}">Detail</a>
                                                            <a class="dropdown-item" href="{{route('invoice_dispute_detail', $des->id)}}">Dispute detail</a>
                                                            @if($my_rights->invoice->edit)
                                                                <a class="dropdown-item" onclick="confirmDelete('{{route('delete_invoice', $des->id)}}')">Delete</a>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </td>

                                            </tr>
                                        @endforeach


                                        </tbody>
                                    </table>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>


        </div>
    </div>
@endsection
<script>
    function confirmDelete(url) {
        var r = confirm("Are you sure, You wants to delete ?");
        if (r == true) {
            window.open(url, "_self");
        }
    }
</script>