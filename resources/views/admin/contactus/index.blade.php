@extends('admin.layout.interface')

@section('bread-cumbs')
    @include('admin.layout.breadcrumbs',
    ['breadcrumbs' =>
        ['title' => 'Stock',
         'items' => [
             ['name' => 'Home', 'url' => url("/"), 'active' => false],
             ['name' => 'Stock', 'url' => url("/"), 'active' => true],

         ]
        ]
    ])
@endsection

@section('content')

    <div class="row">
        <div class="col">
            <div class="card">
                <div class="card-header">
                    @if (session()->has('success') || session()->has('error'))
                        <div class="alert alert-{{session()->has('success') ? 'success' : 'danger'}}">
                            {{session()->has('success') ? session()->get('success') : session()->get('error')}}
                        </div>
                    @endif
                    <h4 class="card-title" id="horz-layout-colored-controls">Contact Us</h4>
                </div>
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Message</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                            @foreach($contactUs as $key=>$cs)
                                <tr>
                                    <td>{{$key+1}}</td>
                                    <td>{{$cs->name}}</td>
                                    <td>{{$cs->email}}</td>
                                    <td>{{$cs->message}}</td>
                                    <td>
                                        <div class="btn-group mr-1 mb-1">
                                            <button class="btn btn-danger btn-sm dropdown-toggle" type="button"
                                                    id="dropdownMenuButton6" data-toggle="dropdown" aria-haspopup="true"
                                                    aria-expanded="false">
                                                Action
                                            </button>
                                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                                    <a class="dropdown-item"
                                                       onclick="confirmDelete('{{ route('delete_contact_us',$cs->id) }}')">Delete</a>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection

<script>
    function confirmDelete(url) {
        var r = confirm("Are you sure, You wants to delete ?");
        if (r == true) {
            window.open(url, "_self");
        }
    }
</script>