@extends('admin.layout.interface')

@section('bread-cumbs')
    @include('admin.layout.breadcrumbs',
    ['breadcrumbs' =>
        ['title' => 'Stock',
         'items' => [
             ['name' => 'Home', 'url' => url("/"), 'active' => false],
             ['name' => 'Stock', 'url' => url("/"), 'active' => true],

         ]
        ]
    ])
@endsection

@section('content')

    <div class="row">
        <div class="col">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title" id="horz-layout-colored-controls">App User Details</h4>
                </div>
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Full Name</th>
                            <th>Phone</th>
                            <th>Email</th>
                            <th>Address</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($users as $k=>$user)
                            <tr>
                                <th scope="row">{{$k+1}}</th>
                                <td>{{$user->f_name.' '.$user->l_name}}</td>
                                <td>{{$user->phone}}</td>
                                <td>{{$user->email}}</td>
                                <td>{{$user->address}}</td>
                                <td>

                                    <div class="btn-group mr-1 mb-1">
                                        <button class="btn btn-danger btn-sm dropdown-toggle" type="button"
                                                id="dropdownMenuButton6" data-toggle="dropdown" aria-haspopup="true"
                                                aria-expanded="false">
                                            Action
                                        </button>
                                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                            <a class="dropdown-item" href="{{route('view_detail',$user->id)}}">View Detail</a>
                                            <a class="dropdown-item" onclick="confirmDelete('{{route('delete_app_user',$user->id)}}')">Delete</a>
                                        </div>
                                    </div>
                                    </i>
                                </td>

                            </tr>
                        @endforeach

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <script>
        function confirmDelete(url) {
            var r = confirm("Are you sure, You wants to delete ?");
            if (r == true) {
                window.open(url, "_self");
            }
        }
    </script>
@endsection
