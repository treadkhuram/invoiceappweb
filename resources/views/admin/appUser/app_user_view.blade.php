@extends('admin.layout.interface')

@section('bread-cumbs')
    @include('admin.layout.breadcrumbs',
    ['breadcrumbs' =>
        ['title' => 'Stock',
         'items' => [
             ['name' => 'Home', 'url' => url("/"), 'active' => false],
             ['name' => 'Stock', 'url' => url("/"), 'active' => true],

         ]
        ]
    ])
@endsection

@section('content')

    <div class="app-content">
        <div class="app-content">
            <div class="content-wrapper-before"></div>
            <div class="content-header row">
                <div class="content-header-left col-md-4 col-12 mb-2">
                    <h3 class="content-header-title">User Detail</h3>
                </div>
            </div>
            <div class="content-body">
                <section class="card">
                    <div id="invoice-template" class="card-body">
                        <!-- Invoice Company Details -->
                        <form action="{{route('update_app_user_detail')}}" method="post">
                            <div id="invoice-company-details" class="row">
                                <div class="col-md-6 col-sm-12 text-left text-md-left">
                                    <h1>User detail</h1>
                                    <table class="col-md-12">
                                        <tbody>
                                            <input type="hidden" name="user_id" value="{{$user->id}}"/>
                                            <tr><td>First Name: </td>
                                                <td><input type="text" class="form-control input-sm" name="f_name" value="{{$user->f_name}}"></td>
                                            </tr>
                                            <tr><td>Last Name: </td>
                                                <td><input type="text" class="form-control input-sm" name="l_name" value="{{$user->l_name}}"></td>
                                            </tr>
                                            <tr><td>Phone: </td>
                                                <td><input type="text" class="form-control input-sm" name="phone" value="{{$user->phone}}"></td>
                                            </tr>
                                            <tr><td>Email: </td>
                                                <td><input type="text" class="form-control input-sm" name="email" value="{{$user->email}}"></td>
                                            </tr>
                                            <tr><td>Address:</td>
                                                <td><input type="text" class="form-control input-sm" name="address" value="{{$user->address}}"></td>
                                            </tr>
                                            <tr><td>City:</td>
                                                <td><input type="text" class="form-control input-sm" name="city" value="{{$user->city}}"></td>
                                            </tr>
                                            <tr><td>Zip:</td>
                                                <td><input type="text" class="form-control input-sm" name="zip" value="{{$user->zip}}"></td>
                                            </tr>
                                            <tr><td>Country:</td>
                                                <td><input type="text" class="form-control input-sm" name="country" value="{{$user->country}}"></td>
                                            </tr>
                                            <tr><td>Verified:</td>
                                                <td>
                                                    <select name="verified" class="form-control input-sm">
                                                        <option {{$user->verified == 0 ? 'selected' : ''}} value="0">No</option>
                                                        <option {{$user->verified == 1 ? 'selected' : ''}} value="1">Yes</option>
                                                    </select>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                @if(isset($user->bank_detail))
                                    <div class="col-md-6 col-sm-12 text-left text-md-left">
                                        <h1>User Bank detail</h1>
                                        <?php $bankDetail = json_decode($user->bank_detail) ?>
                                        <table class="col-md-12">
                                            <tbody>
                                                <tr><td>Bank Name: </td>
                                                    <td><input type="text" class="form-control input-sm" name="bank_name" value="{{$bankDetail->bank_name}}"></td>
                                                </tr>
                                                <tr><td>Bank Phone: </td>
                                                    <td><input type="text" class="form-control input-sm" name="bank_phone" value="{{$bankDetail->bank_phone_no}}"></td>
                                                </tr>
                                                <tr><td>Account#: </td>
                                                    <td><input type="text" class="form-control input-sm" name="account_no" value="{{$bankDetail->account_no}}"></td>
                                                </tr>
                                                <tr><td>Short code: </td>
                                                    <td><input type="text" class="form-control input-sm" name="short_code" value="{{$bankDetail->short_code}}"></td>
                                                </tr>
                                                <tr><td>Address:</td>
                                                    <td><input type="text" class="form-control input-sm" name="bank_address" value="{{$bankDetail->bank_address}}"></td>
                                                </tr>
                                                <tr><td>City:</td>
                                                    <td><input type="text" class="form-control input-sm" name="bank_city" value="{{$bankDetail->bank_city}}"></td>
                                                </tr>
                                                <tr><td>Zip:</td>
                                                    <td><input type="text" class="form-control input-sm" name="bank_zip" value="{{$bankDetail->bank_zip}}"></td>
                                                </tr>
                                                <tr><td>State:</td>
                                                    <td><input type="text" class="form-control input-sm" name="bank_state" value="{{$bankDetail->bank_state}}"></td>
                                                </tr>
                                                <tr><td>Country:</td>
                                                    <td><input type="text" class="form-control input-sm" name="bank_country" value="{{$bankDetail->bank_country}}"></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                @endif
                            </div>
                            @if(\Illuminate\Support\Facades\Session::get('user')->user_type == 'admin')
                                <button class="btn btn-primary float-right mt-2">Update</button>
                            @endif
                        </form>

                    </div>


            </section>
        </div>
    </div>
    </div>

@endsection
