    <!-- BEGIN: Main Menu-->
    <div class="main-menu menu-fixed menu-light menu-accordion    menu-shadow " data-scroll-to-active="true" data-img="app-assets/images/backgrounds/02.jpg">
        <div class="navbar-header">
            <ul class="nav navbar-nav flex-row">
                    <li class="nav-item mr-auto"><a class="navbar-brand" href="{{route('dashboard')}}">
                        <h3 class="brand-text">PayKamsy</h3>
                    </a></li>
                <li class="nav-item d-md-none"><a class="nav-link close-navbar"><i class="ft-x"></i></a></li>
            </ul>
        </div>
        <div class="navigation-background"></div>
        <div class="main-menu-content">
            <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">
                <li class=" nav-item"><a href="{{url('/dashboard')}}"><i class="ft-home"></i><span class="menu-title" data-i18n="">Dashboard</span></a>
                </li>
            </ul>

            {{--{"user":{"view":"1","edit":"1"},
            "app_user":{"view":"0","edit":"0"},
            "invoice":{"view":"1","edit":"1"},
            "invoice_amount":{"view":"1","edit":"1"},
            "dispute":{"view":"1","edit":"1"}}--}}

            {{-- <span class="badge badge badge-info badge-pill float-right mr-2"></span> --}}
            @if($my_rights->user->view == 1 || $my_rights->user->edit == 1)
            <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">
                <li class=" nav-item"><a href="#"><i class="ft-user-check"></i></i><span class="menu-title" data-i18n="">User</span></a>
                    <ul class="menu-content">
                        @if($my_rights->user->edit == 1)
                        <li class="{{Request::url() == route('add.user') ? 'active' : ''}}"><a class="menu-item" href="{{ route('add.user') }}">Add User</a>
                        </li>
                        @endif
                        @if($my_rights->user->view == 1)
                        <li class="{{Request::url() == route('view.user') ? 'active' : ''}}"><a class="menu-item" href="{{ route('view.user') }}">View User</a>
                        </li>
                        @endif
                    </ul>
                </li>
            </ul>
            @endif
            @if($my_rights->app_user->view == 1)
            <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">
                <li class=" nav-item"><a href="{{route('view.appUser')}}"><i class="ft-tablet"></i></i></i><span class="menu-title" data-i18n="">App User</span></a>
                </li>
            </ul>
            @endif
            @if($my_rights->invoice->view == 1)
             <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">
                <li class=" nav-item"><a href="{{route('view.invoice')}}"><i class="ft-file"></i><span class="menu-title" data-i18n="">Invoices</span></a>
                </li>
            </ul>
            @endif
            @if($my_rights->invoice_amount->view == 1)
            <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">
                <li class=" nav-item"><a href="{{route('payment')}}"><i class="ft-credit-card"></i><span class="menu-title" data-i18n="">Invoices payment</span></a>
                </li>
            </ul>
            @endif
            @if($my_rights->dispute->view == 1)
             <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">
                <li class=" nav-item"><a href="{{route('view.dispute')}}"><i class="ft-alert-triangle"></i></i><span class="menu-title" data-i18n="">Dispute</span></a>
                </li>
            </ul>
            @endif
            @if($login_user_type == 'admin')
                <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">
                    <li class=" nav-item"><a href="{{route('bank_detail')}}"><i class="ft-credit-card"></i></i><span class="menu-title" data-i18n="">Bank details</span></a>
                    </li>
                </ul>

                <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">
                    <li class=" nav-item"><a href="{{route('system_logs')}}"><i class="ft-file"></i></i><span class="menu-title" data-i18n="">System Logs</span></a>
                    </li>
                </ul>
            @endif
            @if($my_rights->contactus->view == 1)
            <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">
                <li class=" nav-item"><a href="{{route('contactUs')}}"><i class="ft-message-square"></i><span class="menu-title" data-i18n="">Contact Us</span></a>
                </li>
            </ul>
            @endif

        </div>
    </div>
    <!-- END: Main Menu-->
