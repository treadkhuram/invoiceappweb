 {{-- <!-- BEGIN: Footer--><a class="btn btn-try-builder btn-bg-gradient-x-purple-red btn-glow white" href="https://www.themeselection.com/layout-builder/horizontal" target="_blank">Try Layout Builder</a>
 <footer class="footer footer-static footer-light navbar-border navbar-shadow">
    <div class="clearfix blue-grey lighten-2 text-sm-center mb-0 px-2"><span class="float-md-left d-block d-md-inline-block">2019 &copy; Copyright <a class="text-bold-800 grey darken-2" href="https://themeselection.com" target="_blank">ThemeSelection</a></span>
        <ul class="list-inline float-md-right d-block d-md-inline-blockd-none d-lg-block mb-0">
            <li class="list-inline-item"><a class="my-1" href="https://themeselection.com/" target="_blank"> More themes</a></li>
            <li class="list-inline-item"><a class="my-1" href="https://themeselection.com/support" target="_blank"> Support</a></li>
        </ul>
    </div>
</footer>
<!-- END: Footer-->
</div> --}}



<!-- BEGIN: Vendor JS-->
<script src="{{asset('/assets/admin/')}}/vendors/js/vendors.min.js" type="text/javascript"></script>
<!-- BEGIN Vendor JS-->

<!-- BEGIN: Page Vendor JS-->
<script src="{{asset('/assets/admin/')}}/vendors/js/charts/chartist.min.js" type="text/javascript"></script>
<script src="{{asset('/assets/admin/')}}/vendors/js/charts/chartist-plugin-tooltip.min.js" type="text/javascript"></script>
<!-- END: Page Vendor JS-->

<!-- BEGIN: Theme JS-->
<script src="{{asset('/assets/admin/')}}/js/core/app-menu.js" type="text/javascript"></script>
<script src="{{asset('/assets/admin/')}}/js/core/app.js" type="text/javascript"></script>
<!-- END: Theme JS-->

<!-- BEGIN: Page JS-->
<script src="{{asset('/assets/admin/')}}/js/scripts/pages/dashboard-analytics.js" type="text/javascript"></script>
<!-- END: Page JS-->

<script src="{{asset('/assets/admin/js/custom.js')}}"> </script>
<script>
    $('#myTable').DataTable({

    rowReorder: {
        selector: 'td:nth-child(2)'
    },
    // responsive: true,

    responsive: {
    details: {
        type: 'column'
    }
    },
    columnDefs: [ {
        className: 'dtr-control',
        orderable: false,
        targets:   0
    } ],
    order: [ 1, 'asc' ]
});
</script>
</body>
<!-- END: Body-->

</html>
