@extends('admin.layout.interface')

@section('bread-cumbs')
    @include('admin.layout.breadcrumbs',
    ['breadcrumbs' =>
        ['title' => 'Stock',
         'items' => [
             ['name' => 'Home', 'url' => url("/"), 'active' => false],
             ['name' => 'Stock', 'url' => url("/"), 'active' => true],

         ]
        ]
    ])
@endsection

@section('content')

    <div class="row">
        <div class="col">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title" id="horz-layout-colored-controls">User Logs</h4>
                </div>
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Type</th>
                            <th>Detail</th>
                            <th>User</th>
                            <th>Time</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                            @foreach($logs as $k=>$log)
                                <tr>
                                    <td>{{$k+1}}</td>
                                    <td>{{$log->actions}}</td>
                                    <td>{{$log->action_detail}}</td>
                                    <td>{{$log->user->name}}</td>
                                    <td>{{date('d/m/Y h:i', strtotime($log->created_at))}}</td>
                                    <td>
                                        <a class="dropdown-item" href="{{route('delete_log', $log->id)}}" ><i class="text-primary ft-check"></i></a>
                                    </td>
                                </tr>
                            @endforeach
                        <tr><td colspan="6" ><a class="float-right" href="{{route('delete_log', -1)}}"><i class="text-primary ft-check"></i>Close all</a></td></tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
