@extends('admin.layout.interface')

@section('bread-cumbs')
    @include('admin.layout.breadcrumbs',
    ['breadcrumbs' =>
        ['title' => 'Dashbaord',
         'items' => [
             ['name' => 'Home', 'url' => url("/"), 'active' => false],
             ['name' => 'Add', 'url' => url("/"), 'active' => false],
             ['name' => 'Demo', 'url' => null, 'active' => true]
         ]
        ]
    ])
@endsection

@section('content')

    <div class="row">
        <div class="col">
            <div class="card">
                <div class="card-header">
                    Dashboard
                </div>

                <div class="row">

                     <div class="col-md-12 col-lg-4">
                        <div class="card pull-up border-top-info border-top-3 rounded-0">
                            <div class="card-header">
                                <h4 class="card-title">App User </h4>
                            </div>
                            <div class="card-content collapse show">
                                <div class="card-body p-1">
                                    <h4 class="font-large-1 text-bold-400">{{$appUsers}} <i class="ft-users float-right"></i></h4>
                                </div>
                                <div class="card-footer p-1">
                                    
                                </div>
                            </div>
                        </div>
                    </div>

                     <div class="col-md-12 col-lg-4">
                        <div class="card pull-up border-top-info border-top-3 rounded-0">
                            <div class="card-header">
                                <h4 class="card-title">New Invoices </h4>
                            </div>
                            <div class="card-content collapse show">
                                <div class="card-body p-1">
                                    <h4 class="font-large-1 text-bold-400">{{$invoices}} <i class="ft-file float-right"></i></h4>
                                </div>
                                <div class="card-footer p-1">
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                     <div class="col-md-12 col-lg-4">
                        <div class="card pull-up border-top-info border-top-3 rounded-0">
                            <div class="card-header">
                                <h4 class="card-title">New Dispute </h4>
                            </div>
                            <div class="card-content collapse show">
                                <div class="card-body p-1">
                                    <h4 class="font-large-1 text-bold-400">{{$disputes}}<i class="ft-alert-triangle float-right"></i></h4>
                                </div>
                                <div class="card-footer p-1">
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                    
                </div>

            </div>
        </div>
    </div>
@endsection
