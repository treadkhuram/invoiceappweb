@extends('admin.layout.interface')
@include('admin.layout.breadcrumbs',
['breadcrumbs' =>
    ['title' => 'Dashbaord',
     'items' => [
         ['name' => 'Home', 'url' => url("/"), 'active' => false],
         ['name' => 'Add', 'url' => url("/"), 'active' => false],
         ['name' => 'Demo', 'url' => null, 'active' => true]
     ]
    ]
])
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title" id="horz-layout-colored-controls">Bank detail</h4>
                </div>
                <div class="card-content collpase show">
                    <div class="card-body">
                        @if (session()->has('success') || session()->has('error'))
                            <div class="alert alert-{{session()->has('success') ? 'success' : 'danger'}}">
                                {{session()->has('success') ? session()->get('success') : session()->get('error')}}
                            </div>
                        @endif
                        <form class="form form-horizontal" action="{{route('bank_detail')}}" method="POST">
                            @csrf
                            <div class="form-body">
                                <h4 class="form-section"><i class="la la-eye"></i> PayKamsy bank detail</h4>
                                <div class="row">
                                    <div class=" offset-md-2 col-md-6">
                                        <div class="form-group row">
                                            <label class="col-md-3 label-control" for="userinput1">Bank Name</label>
                                            <div class="col-md-9">
                                                <input type="hidden" name='id' >
                                                <input type="text" id="name" name="bank_name" value="{{$bank_account->bank_name}}" class="form-control border-primary" >
                                            </div>
                                        </div>
                                    </div>
                                    <div class="offset-md-2 col-md-6">
                                        <div class="form-group row">
                                            <label class="col-md-3 label-control" for="userinput3">Account No</label>
                                            <div class="col-md-9">
                                                <input type="text" class="form-control border-primary" value="{{$bank_account->account_no}}" name="account_no" >
                                            </div>
                                        </div>
                                    </div>
                                    <div class="offset-md-2 col-md-6">
                                        <div class="form-group row">
                                            <label class="col-md-3 label-control" for="userinput2">Short code</label>
                                            <div class="col-md-9">
                                                <input type="text" class="form-control border-primary" value="{{$bank_account->short_code}}" name="short_code" >
                                            </div>
                                        </div>
                                    </div>
                                    <div class="offset-md-2 col-md-6">
                                        <div class="form-group row">
                                            <label class="col-md-3 label-control" for="userinput4">Phone</label>
                                            <div class="col-md-9">
                                                <input type="text" class="form-control border-primary" name="phone" value="{{$bank_account->phone}}" >
                                            </div>
                                        </div>
                                    </div>

                                    <div class="offset-md-2 col-md-6">
                                        <div class="form-group row">
                                            <label class="col-md-3 label-control" for="userinput4">City</label>
                                            <div class="col-md-9">
                                                <input type="text" class="form-control border-primary" name="city" value="{{$bank_account->city}}" >
                                            </div>
                                        </div>
                                    </div>
                                    <div class="offset-md-2 col-md-6">
                                        <div class="form-group row">
                                            <label class="col-md-3 label-control" for="userinput4">State</label>
                                            <div class="col-md-9">
                                                <input type="text" class="form-control border-primary" name="state" value="{{$bank_account->state}}" >
                                            </div>
                                        </div>
                                    </div>
                                    <div class="offset-md-2 col-md-6">
                                        <div class="form-group row">
                                            <label class="col-md-3 label-control" for="userinput4">Zip</label>
                                            <div class="col-md-9">
                                                <input type="text" class="form-control border-primary" name="zip" value="{{$bank_account->zip}}" >
                                            </div>
                                        </div>
                                    </div>
                                    <div class="offset-md-2 col-md-6">
                                        <div class="form-group row">
                                            <label class="col-md-3 label-control" for="userinput4">Country</label>
                                            <div class="col-md-9">
                                                <input type="text" class="form-control border-primary" name="country" value="{{$bank_account->country}}">
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>

                            <div class="form-actions right">
                                <button type="submit" name="btn_submit" class="btn btn-primary">
                                    <i class="la la-check-square-o"></i> Update
                                </button>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
@endsection
