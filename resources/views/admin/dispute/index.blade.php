@extends('admin.layout.interface')

@section('bread-cumbs')
    @include('admin.layout.breadcrumbs',
    ['breadcrumbs' =>
        ['title' => 'Stock',
         'items' => [
             ['name' => 'Home', 'url' => url("/"), 'active' => false],
             ['name' => 'Stock', 'url' => url("/"), 'active' => true],

         ]
        ]
    ])
@endsection

@section('content')

    <div class="row">
        <div class="col">

            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">Disputes</h4>
                </div>
                <div class="card-content">
                    <div class="card-body">
                        <ul class="nav nav-tabs">
                            <li class="nav-item">
                                <a class="nav-link active" id="base-tab1" data-toggle="tab" aria-controls="tab1" href="#tab1" aria-expanded="true">New</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="base-tab2" data-toggle="tab" aria-controls="tab2" href="#tab2" aria-expanded="false">In-process</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="base-tab3" data-toggle="tab" aria-controls="tab3" href="#tab3" aria-expanded="false">Completed</a>
                            </li>
                        </ul>
                        <div class="tab-content px-1 pt-1">
                            <div role="tabpanel" class="tab-pane active" id="tab1" aria-expanded="true" aria-labelledby="base-tab1">
                                <div class="table-responsive">
                                    <table class="table">
                                        <thead>
                                        <tr>
                                            <th>Invoice#</th>
                                            <th>Title</th>
                                            <th>Description</th>
                                            <th>Notes</th>
                                            <th>Action</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($pending as $pe)
                                            <tr>
                                                <td>{{$pe->invoice_id}}</td>
                                                <td>{{$pe->title}}</td>
                                                <td>{{$pe->description}}</td>
                                                <td>{{$pe->notes}}</td>
                                                <td>
                                                    <div class="btn-group mr-1 mb-1">
                                                        <button class="btn btn-danger btn-sm dropdown-toggle" type="button" id="dropdownMenuButton6" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                            Action
                                                        </button>
                                                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                                            @if($my_rights->dispute->edit)
                                                                <a class="dropdown-item" href="{{route('dispute_detail', $pe->id)}}">View</a>
                                                                <a class="dropdown-item" href="{{route('change_status',['id'=>$pe->id, 'status'=>'in-process'])}}">In-process</a>
                                                                <a class="dropdown-item" href="{{route('change_status',['id'=>$pe->id, 'status'=>'complete'])}}">Complete</a>
                                                                <a class="dropdown-item" onclick="confirmDelete('{{route('delete_dispute', $pe->id)}}')">Delete</a>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>

                            </div>
                            <div class="tab-pane" id="tab2" aria-labelledby="base-tab2">
                                <div class="table-responsive">
                                    <table class="table">
                                        <thead>
                                        <tr>
                                            <th>Invoice#</th>
                                            <th>Title</th>
                                            <th>Description</th>
                                            <th>Notes</th>
                                            <th>Action</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($process as $pe)
                                            <tr>
                                                <td>{{$pe->invoice_id}}</td>
                                                <td>{{$pe->title}}</td>
                                                <td>{{$pe->description}}</td>
                                                <td>{{$pe->notes}}</td>
                                                <td>
                                                    <div class="btn-group mr-1 mb-1">
                                                        <button class="btn btn-danger btn-sm dropdown-toggle" type="button" id="dropdownMenuButton6" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                            Action
                                                        </button>
                                                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                                            @if($my_rights->dispute->edit)
                                                                <a class="dropdown-item" href="{{route('change_status',['id'=>$pe->id, 'status'=>'complete'])}}">Complete</a>
                                                                <a class="dropdown-item" onclick="confirmDelete('{{route('delete_dispute', $pe->id)}}')">Delete</a>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="tab-pane" id="tab3" aria-labelledby="base-tab3">
                                <div class="table-responsive">
                                    <table class="table">
                                        <thead>
                                        <tr>
                                            <th>Invoice#</th>
                                            <th>Title</th>
                                            <th>Description</th>
                                            <th>Notes</th>
                                            <th>Action</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($complete as $pe)
                                            <tr>
                                                <td>{{$pe->invoice_id}}</td>
                                                <td>{{$pe->title}}</td>
                                                <td>{{$pe->description}}</td>
                                                <td>{{$pe->notes}}</td>
                                                <td>
                                                    <div class="btn-group mr-1 mb-1">
                                                        <button class="btn btn-danger btn-sm dropdown-toggle" type="button" id="dropdownMenuButton6" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                            Action
                                                        </button>
                                                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                                            @if($my_rights->dispute->edit)
                                                                <a class="dropdown-item" onclick="confirmDelete('{{route('delete_dispute', $pe->id)}}')">Delete</a>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


        </div>
    </div>
    <script>
        function confirmDelete(url) {
            var r = confirm("Are you sure, You wants to delete ?");
            if (r == true) {
                window.open(url, "_self");
            }
        }
    </script>
@endsection
