@extends('admin.layout.interface')

@section('bread-cumbs')
    @include('admin.layout.breadcrumbs',
    ['breadcrumbs' =>
        ['title' => 'Stock',
         'items' => [
             ['name' => 'Home', 'url' => url("/"), 'active' => false],
             ['name' => 'Stock', 'url' => url("/"), 'active' => true],

         ]
        ]
    ])
@endsection

@section('content')

 <div class="app-content">
        <div class="app-content">
            <div class="content-wrapper-before"></div>
            <div class="content-header row">
                <div class="content-header-left col-md-4 col-12 mb-2">
                    <h3 class="content-header-title">Dispute Detail</h3>
                </div>
                <div class="content-header-right col-md-8 col-12">
                    <div class="breadcrumbs-top float-md-right">
                        <div class="breadcrumb-wrapper mr-1">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="index.html">Dispute</a>
                                </li>
                                
                                <li class="breadcrumb-item active">Dispute detail
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
            <div class="content-body">
                <section class="card">
                    <div id="invoice-template" class="card-body">
                        <!-- Invoice Company Details -->
                        <div id="invoice-company-details" class="row">
                            <div class="col-md-6 col-sm-12 text-left text-md-left">
                               <!--  <img src="app-assets/images/logo/logo-80x80.png" alt="company logo" class="mb-2" width="70"> -->
                                <ul class="px-0 list-unstyled">
                                    <li><h1>Invoice# {{$dispute->invoice_id}}</h1></li>
                                    <li class="text-bold-700">Name: {{$user->f_name.' '.$user->l_name}}</li>
                                    <li>Phone: {{$user->phone}}</li>
                                    <li>Email: {{$user->email}}</li>
                                </ul>

                            </div>
                            
                        </div>

                        <!-- Invoice Customer Details -->
                        <div id="invoice-customer-details" class="row pt-2">
                            <div class="col-md-6 col-sm-12">
                                <p class="text-muted"><b> Title: {{$dispute->title}}</b></p>
                            </div>
                          
                        </div>
                        <!-- Invoice Items Details -->
                        <div id="invoice-items-details" class="pt-2">

                             
                        </div>
                          <h3>Dispute Detail</h3>

                          <p>{{$dispute->description}}</p>


                        <div class="mt-5">
                            <div class="row">
                                <h3 class="col-md-6">Add Notes</h3>
                                @if(isset($dispute->edited_by))
                                    <span>edited by: {{$editUser->name}}</span>
                                    &nbsp&nbsp&nbsp&nbsp&nbsp
                                    <span> at: {{date('d-m-Y h:m', strtotime($dispute->edit_time))}}</span>
                                @endif

                            </div>
                            <form action="{{url('/dispute/add_notes')}}" method="post">
                                <input type="hidden" name="id" value="{{$dispute->id}}">
                                <textarea class="col-md-12" name="notes" rows="5">{{$dispute->notes}}</textarea>
                                <button class="btn btn-primary float-right">Save</button>
                            </form>
                        </div>


                        </div>


                        

                    </div>
                </section>
            </div>
        </div>
    </div>

    @endsection
