@extends('admin.layout.interface')
@include('admin.layout.breadcrumbs',
['breadcrumbs' =>
    ['title' => 'Dashbaord',
     'items' => [
         ['name' => 'Home', 'url' => url("/"), 'active' => false],
         ['name' => 'Add', 'url' => url("/"), 'active' => false],
         ['name' => 'Demo', 'url' => null, 'active' => true]
     ]
    ]
])
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title" id="horz-layout-colored-controls">Add User</h4>
                </div>
                <div class="card-content collpase show">
                    <div class="card-body">
                        @if (session()->has('success') || session()->has('error'))
                            <div class="alert alert-{{session()->has('success') ? 'success' : 'danger'}}">
                                {{session()->has('success') ? session()->get('success') : session()->get('error')}}
                            </div>
                        @endif
                        <form class="form form-horizontal" action="{{route('update_profile')}}" method="POST">
                            @csrf
                            <div class="form-body">
                                <h4 class="form-section"><i class="la la-eye"></i> User Details</h4>
                                <div class="row">
                                    <div class=" offset-md-2 col-md-6">
                                        <div class="form-group row">
                                            <label class="col-md-3 label-control" for="userinput1">Full Name</label>
                                            <div class="col-md-9">
                                                <input type="hidden" name='id' value=<?= $user->id?>>
                                                <input type="text" id="name" name="name" class="form-control border-primary" value='<?= $user->name?>' required>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="offset-md-2 col-md-6">
                                        <div class="form-group row">
                                            <label class="col-md-3 label-control" for="userinput3">Phone#</label>
                                            <div class="col-md-9">
                                                <input type="text" class="form-control border-primary" value='<?= $user->phone?>' name="phone" required>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="offset-md-2 col-md-6">
                                        <div class="form-group row">
                                            <label class="col-md-3 label-control" for="userinput2">Address</label>
                                            <div class="col-md-9">
                                                <input type="text"  class="form-control border-primary" value='<?= $user->address?>' name="address" required>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="offset-md-2 col-md-6">
                                        <div class="form-group row">
                                            <label class="col-md-3 label-control" for="userinput4">Email</label>
                                            <div class="col-md-9">
                                                <input type="Email" class="form-control border-primary" value='<?= $user->email?>' name="email" readonly>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="offset-md-2 col-md-6">
                                        <label class="col-md-6 text-danger">Change Password</label>
                                        <div class="form-group row">
                                            <label class="col-md-3 label-control" for="userinput5">New Password</label>
                                            <div class="col-md-9">
                                                <input onkeyup="checkPassword()" class="form-control border-primary" type="Password" id="new_password" name="password">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="offset-md-2 col-md-6">
                                        <div class="form-group row">
                                            <label class="col-md-3 label-control" for="userinput5">Confirm Password</label>
                                            <div class="col-md-9">
                                                <input onkeyup="checkPassword()" class="form-control border-primary" type="Password" id="c_password" name="cpassword">
                                                <label id="password_label" class="text-danger" ></label>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>

                            <div class="form-actions right">
                                <button type="submit" id="btn_submit" class="btn btn-primary">
                                    <i class="la la-check-square-o"></i> Update
                                </button>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>

        function checkPassword() {
            if ($('#new_password').val() != $('#c_password').val()) {
                $('#password_label').html('Password did not match');
                $('#btn_submit').prop('disabled', true);
            } else {
                $('#password_label').html('');
                $('#btn_submit').removeAttr('disabled');
            }
            if($('#c_password').val() == ''){
                $('#password_label').html('');
            }
        }
    </script>
@endsection
