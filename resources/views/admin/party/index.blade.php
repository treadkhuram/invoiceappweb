@extends('admin.layout.interface')

@section('bread-cumbs')
    @include('admin.layout.breadcrumbs',
    ['breadcrumbs' =>
        ['title' => 'Stock',
         'items' => [
             ['name' => 'Home', 'url' => url("/"), 'active' => false],
             ['name' => 'Stock', 'url' => url("/"), 'active' => true],

         ]
        ]
    ])
@endsection

@section('content')

    <div class="row">
        <div class="col">
            <div class="card">
                <div class="card-header">
                    Stock
                </div>
            </div>
        </div>
    </div>
@endsection
