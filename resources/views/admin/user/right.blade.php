@extends('admin.layout.interface')

@section('bread-cumbs')
    @include('admin.layout.breadcrumbs',
    ['breadcrumbs' =>
        ['title' => 'Stock',
         'items' => [
             ['name' => 'Home', 'url' => url("/"), 'active' => false],
             ['name' => 'Stock', 'url' => url("/"), 'active' => true],

         ]
        ]
    ])
@endsection

@section('content')

    <div class="row">
        <div class="col">
            <div class="card">
                <div class="card-header">
                    @if (session()->has('success') || session()->has('error'))
                        <div class="alert alert-{{session()->has('success') ? 'success' : 'danger'}}">
                            {{session()->has('success') ? session()->get('success') : session()->get('error')}}
                        </div>
                    @endif
                    <h4 class="card-title" id="horz-layout-colored-controls">User Rights</h4>
                </div>
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Functions</th>
                            <th>Can View</th>
                            <th>Can Edit</th>

                        </tr>
                        </thead>
                        <tbody>
                        <div class="row">

                            <div class="card-content collpase show">
                                <div class="card-body">

                                    <form class="form form-horizontal" action="{{route('save.edit.right')}}"
                                          method='post'>
                                        @csrf
                                        <div class="form-body">
                                            <?php foreach($EditData as $edit)
                                            {
                                            $data = json_decode($edit->user_rights);

                                            ?>


                                            <input type="hidden" name='id' value=<?= $edit->id?>>
                                            <?php  }

                                            ?>
                                            <tr>
                                                <th>1</th>
                                                <th>User</th>
                                                <?php if($data == null){?>
                                                <th><input type="checkbox" name='viewuser' value='1'>
                                                <th><input type="checkbox" name='edituser' value='1'>
                                                <?php } else{?>
                                                <?php if($data->user->view != 0){?>
                                                <th><input type="checkbox" name='viewuser' value='1' checked>
                                                <?php } else{?>
                                                <th><input type="checkbox" name='viewuser' value='1'>
                                                <?php } if($data->user->edit != 0){?>
                                                <th><input type="checkbox" name='edituser' value='1' checked>
                                                <?php } else{?>
                                                <th><input type="checkbox" name='edituser' value='1'>
                                                <?php } } ?>
                                            </tr>
                                            <tr>
                                                <th>2</th>
                                                <th>App User</th>
                                                <?php if($data == null){?>
                                                <th><input type="checkbox" name='viewappuser' value='1'>
                                                <th><input type="checkbox" name='editappuser' value='1'>
                                                <?php } else{?>
                                                <?php if($data->app_user->view != 0){?>
                                                <th><input type="checkbox" name='viewappuser' value='1' checked>
                                                <?php } else{?>
                                                <th><input type="checkbox" name='viewappuser' value='1'>
                                                <?php } if($data->app_user->edit != 0){?>
                                                <th><input type="checkbox" name='editappuser' value='1' checked>
                                                <?php } else{?>
                                                <th><input type="checkbox" name='editappuser' value='1'>
                                                <?php } } ?>
                                            </tr>
                                            <tr>
                                                <th>3</th>
                                                <th>Invoices</th>
                                                <?php if($data == null){?>
                                                <th><input type="checkbox" name='viewinvoice' value='1'>
                                                <th><input type="checkbox" name='editinvoice' value='1'>
                                                <?php } else{?>
                                                <?php if($data->invoice->view != 0){?>
                                                <th><input type="checkbox" name='viewinvoice' value='1' checked>
                                                <?php } else{?>
                                                <th><input type="checkbox" name='viewinvoice' value='1'>
                                                <?php } if($data->invoice->edit != 0){?>
                                                <th><input type="checkbox" name='editinvoice' value='1' checked>
                                                <?php } else{?>
                                                <th><input type="checkbox" name='editinvoice' value='1'>
                                                <?php } } ?>
                                            </tr>
                                            <tr>
                                                <th>4</th>
                                                <th>Invoices Amount</th>
                                                <?php if($data == null){?>
                                                <th><input type="checkbox" name='viewinvoice_amount' value='1'>
                                                <th><input type="checkbox" name='editinvoice_amount' value='1'>
                                                <?php } else{?>
                                                <?php if($data->invoice_amount->view != 0){?>
                                                <th><input type="checkbox" name='viewinvoice_amount' value='1' checked>
                                                <?php } else{?>
                                                <th><input type="checkbox" name='viewinvoice_amount' value='1'>
                                                <?php } if($data->invoice_amount->edit != 0){?>
                                                <th><input type="checkbox" name='editinvoice_amount' value='1' checked>
                                                <?php } else{?>
                                                <th><input type="checkbox" name='editinvoice_amount' value='1'>
                                                <?php } } ?>
                                            </tr>
                                            <tr>
                                                <th>5</th>
                                                <th>Dispute</th>
                                                <?php if($data == null){?>
                                                <th><input type="checkbox" name='viewdispute' value='1'>
                                                <th><input type="checkbox" name='editdispute' value='1'>
                                                <?php } else{?>
                                                <?php if($data->dispute->view != 0){?>
                                                <th><input type="checkbox" name='viewdispute' value='1' checked>
                                                <?php } else{?>
                                                <th><input type="checkbox" name='viewdispute' value='1'>
                                                <?php } if($data->dispute->edit != 0){?>
                                                <th><input type="checkbox" name='editdispute' value='1' checked>
                                                <?php } else{?>
                                                <th><input type="checkbox" name='editdispute' value='1'>
                                                <?php } } ?>

                                            </tr>
                                                <tr>
                                                    <th>6</th>
                                                    <th>Contact Us</th>
                                                    <?php if($data == null){?>
                                                    <th><input type="checkbox" name='viewContactUs' value='1'>
                                                    <th><input type="checkbox" name='editContactUs' value='1'>
                                                    <?php } else{?>
                                                    <?php if($data->contactus->view != 0){?>
                                                    <th><input type="checkbox" name='viewContactUs' value='1' checked>
                                                    <?php } else{?>
                                                    <th><input type="checkbox" name='viewContactUs' value='1'>
                                                    <?php } if($data->contactus->edit != 0){?>
                                                    <th><input type="checkbox" name='editContactUs' value='1' checked>
                                                    <?php } else{?>
                                                    <th><input type="checkbox" name='editContactUs' value='1'>
                                                    <?php } } ?>

                                                </tr>
                                        </div>
                                        <tr>
                                            <td>
                                            <td>
                                            <td>
                                            <td>
                                                <div class="form-actions right">
                                                    <button type="submit" class="btn btn-primary">
                                                        <i class="la la-check-square-o"></i> Save
                                                    </button>
                                                </div>
                                            </td>
                                        </tr>
                                    </form>

                        </tbody>

                    </table>
                </div>


            </div>
        </div>
    </div>
@endsection

