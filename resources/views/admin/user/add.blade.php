@extends('admin.layout.interface')
@include('admin.layout.breadcrumbs',
['breadcrumbs' =>
    ['title' => 'Dashbaord',
     'items' => [
         ['name' => 'Home', 'url' => url("/"), 'active' => false],
         ['name' => 'Add', 'url' => url("/"), 'active' => false],
         ['name' => 'Demo', 'url' => null, 'active' => true]
     ]
    ]
])
@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title" id="horz-layout-colored-controls">Add User</h4>
            </div>
            <div class="card-content collpase show">
                <div class="card-body">
                    @if (session()->has('success') || session()->has('error'))
                        <div class="alert alert-{{session()->has('success') ? 'success' : 'danger'}}">
                            {{session()->has('success') ? session()->get('success') : session()->get('error')}}
                        </div>
                    @endif
                    <form class="form form-horizontal" action="" method="POST">
                        @csrf
                        <div class="form-body">
                            <h4 class="form-section"><i class="la la-eye"></i> User Details</h4>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <label class="col-md-3 label-control" for="userinput1">Full Name</label>
                                        <div class="col-md-9">
                                            <input type="text" id="name" name="name" class="form-control border-primary" placeholder="Name" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <label class="col-md-3 label-control" for="userinput3">Phone#</label>
                                        <div class="col-md-9">
                                            <input type="text" class="form-control border-primary" placeholder="Phone" name="phone" required>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <label class="col-md-3 label-control" for="userinput2">Address</label>
                                        <div class="col-md-9">
                                            <input type="text"  class="form-control border-primary" placeholder="Address" name="address" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <label class="col-md-3 label-control" for="userinput4">Email</label>
                                        <div class="col-md-9">
                                            <input type="Email" class="form-control border-primary" placeholder="Email" name="email" required>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <label class="col-md-3 label-control" for="userinput5">Password</label>
                                        <div class="col-md-9">
                                            <input class="form-control border-primary" type="Password" placeholder="Password" name="password" required>
                                        </div>
                                    </div>
                                </div>
                               
                            </div>
                         
                        </div>

                        <div class="form-actions right">
                            <button type="submit" class="btn btn-primary">
                                <i class="la la-check-square-o"></i> Save
                            </button>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
