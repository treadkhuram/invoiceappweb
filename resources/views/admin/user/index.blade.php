@extends('admin.layout.interface')

@section('bread-cumbs')
    @include('admin.layout.breadcrumbs',
    ['breadcrumbs' =>
        ['title' => 'Stock',
         'items' => [
             ['name' => 'Home', 'url' => url("/"), 'active' => false],
             ['name' => 'Stock', 'url' => url("/"), 'active' => true],

         ]
        ]
    ])
@endsection

@section('content')

    <div class="row">
        <div class="col">
            <div class="card">
                <div class="card-header">
                    @if (session()->has('success') || session()->has('error'))
                        <div class="alert alert-{{session()->has('success') ? 'success' : 'danger'}}">
                            {{session()->has('success') ? session()->get('success') : session()->get('error')}}
                        </div>
                    @endif
                    <h4 class="card-title" id="horz-layout-colored-controls">User Details</h4>
                </div>
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Full Name</th>
                            <th>Phone</th>
                            <th>Address</th>
                            <th>Email</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach($userData['data'] as $i=>$user )
                        { ?>
                        <tr>
                            <th scope="row"><?= ++$i?></th>
                            <td><?= $user->name?></td>
                            <td><?= $user->phone?></td>
                            <td><?= $user->address?></td>
                            <td><?= $user->email?></td>
                            @if($user->id != 1)
                                <td>
                                    <div class="btn-group mr-1 mb-1">
                                        <button class="btn btn-danger btn-sm dropdown-toggle" type="button"
                                                id="dropdownMenuButton6" data-toggle="dropdown" aria-haspopup="true"
                                                aria-expanded="false">
                                            Action
                                        </button>
                                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                            @if($my_rights->user->edit)
                                                <a class="dropdown-item"
                                                   href='{{ route("edit.user",$user->id) }}'>Edit</a>
                                                <a class="dropdown-item" href='{{ route("right.user",$user->id) }}'>User
                                                    rights</a>
                                                <a class="dropdown-item"
                                                   onclick="confirmDelete('{{ route('delete.user',$user->id) }}')">Delete</a>
                                            @endif
                                        </div>
                                    </div>
                                </td>
                            @endif
                        </tr>
                        <?php } ?>


                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection

<script>
    function confirmDelete(url) {
        var r = confirm("Are you sure, You wants to delete ?");
        if (r == true) {
            window.open(url, "_self");
        }
    }
</script>