<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />

    <title>PayKamsy</title>

    <!-- Favicon -->
    <link rel="icon" href="./images/favicon.png" type="image/x-icon" />

    <!-- Invoice styling -->
    <style>
        body {
            font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
            text-align: center;
            color: #777;
        }

        body h1 {
            font-weight: 300;
            margin-bottom: 0px;
            padding-bottom: 0px;
            color: #000;
        }

        body h3 {
            font-weight: 300;
            margin-top: 10px;
            margin-bottom: 20px;
            font-style: italic;
            color: #555;
        }

        body a {
            color: #06f;
        }

        .invoice-box {
            max-width: 800px;
            margin: auto;
            padding: 10px 20px 10px 30px;
            border: 1px solid #eee;
            box-shadow: 0 0 10px rgba(0, 0, 0, 0.15);
            font-size: 16px;
            line-height: 24px;
            font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
            color: #555;
        }

        .invoice-box table {
            width: 100%;
            line-height: inherit;
            text-align: left;
            border-collapse: collapse;
        }

        .invoice-box table td {
            padding: 5px;
            vertical-align: top;
        }

        .invoice-box table tr td:nth-child(2) {
            text-align: right;
        }

        .invoice-box table tr.top table td {
            padding-bottom: 20px;
        }

        .invoice-box table tr.top table td.title {
            font-size: 45px;
            line-height: 45px;
            color: #333;
        }

        .invoice-box table tr.information table td {
            /*padding-bottom: 40px;*/
        }

        .invoice-box table tr.heading td {
            background: #eee;
            border-bottom: 1px solid #ddd;
            font-weight: bold;
        }

        .invoice-box table tr.details td {
            padding-bottom: 20px;
        }

        .invoice-box table tr.item td {
            border-bottom: 1px solid #eee;
        }

        .invoice-box table tr.item.last td {
            border-bottom: none;
        }

        .invoice-box table tr.total td:nth-child(2) {
            border-top: 2px solid #eee;
            font-weight: bold;
        }

        @media only screen and (max-width: 600px) {
            .invoice-box table tr.top table td {
                width: 100%;
                display: block;
                text-align: center;
            }

            .invoice-box table tr.information table td {
                width: 100%;
                display: block;
                text-align: center;
            }
        }
    </style>
</head>

<body>

<div class="invoice-box">
    <table>
        <tr class="top">
            <td colspan="2">
                <table>
                    <tr>
                        <td class="title">
                            <img src="{{public_path('assets/admin/images/logo/logo.png')}}" alt="logo" style="height: 90px; max-width: 300px" />
                        </td>

                        <td>
                            Invoice #: {{$invoice->invoice_no}}<br />
                            Created: {{date('M-d-Y', strtotime($invoice->created_at))}}<br />
                            Due: {{date('M-d-Y', strtotime($invoice->expiry_date))}}
                        </td>
                    </tr>
                </table>
            </td>
        </tr>

        <tr class="information">
            <td colspan="2">
                <table>
                    <tr>
                        <td>
                            Name: {{$user->full_name}}<br />
                            Email: {{$user->email}}<br />
                            Phone: {{$user->phone}}<br />
                            Address: {{$user->address}}
                        </td>

                        <td>
                            City: {{$user->city}}<br />
                            State: {{$user->state}}<br />
                            Zip: {{$user->zip}}<br />
                            Country: {{$user->country}}
                        </td>
                    </tr>
                </table>

            </td>
        <tr>
            <td colspan="2" >
                Invoice Name: {{$invoice->name}}<br />
                Description: {{$invoice->detail}}<br/><br/>
            </td>
        </tr>
        </tr>
        @if($invoice->invoice_for == '0')
            <tr class="heading">
                <td>Invoice for:</td>
                <td>Goods</td>
            </tr>
            <tr class="item">
                <td>Brand Name</td>
                <td>{{$gsd->brand_name}}</td>
            </tr>
            <tr class="item">
                <td>Model No</td>
                <td>{{$gsd->model_no}}</td>
            </tr>
            <tr class="item">
                <td>Serial No</td>
                <td>{{$gsd->serial_no}}</td>
            </tr>
            <tr class="item">
                <td>Manufacture year</td>
                <td>{{$gsd->year_of_manufacture}}</td>
            </tr>
            <tr class="item">
                <td>Product condition</td>
                <td>{{$gsd->product_condition}}</td>
            </tr>
            <tr class="item">
                <td>Extra links</td>
                <td>{{$gsd->extra_link}}</td>
            </tr>
        @endif
        @if($invoice->invoice_for == '1')
            <tr class="heading">
                <td>Invoice for:</td>
                <td>Services</td>
            </tr>
            <tr class="item">
                <td>Service Title</td>
                <td>{{$gsd->service_title}}</td>
            </tr>
            <tr class="item">
                <td>Service Description</td>
                <td>{{$gsd->service_description}}</td>
            </tr>
        @endif
        @if($invoice->delivery_require == 'yes')
            <tr class="heading">
                <td>Delivery options Require</td>
                <td>Yes</td>
            </tr>
            <tr class="item">
                <td>Delivery Days</td>
                <td>{{$invoice->delivery_days}}</td>
            </tr>
            <tr class="item">
                <td>Shipment cost</td>
                <td>{{$invoice->shipment_cost}}</td>
            </tr>
        @endif
        @if($invoice->delivery_require == 'no')
            <tr class="heading">
                <td>Delivery options Require</td>
                <td>No</td>
            </tr>
            <tr class="item">
                <td>Contract end in day's</td>
                <td>{{$invoice->end_contract}}</td>
            </tr>
            <tr class="item">
                <td>Who pay service charges</td>
                <td>{{$invoice->pay_service_charges}}</td>
            </tr>
        @endif
        <tr class="heading">
            <td>PayKamsy Bank detail</td>
            <td></td>
        </tr>
        <tr class="item">
            <td>Bank Name</td>
            <td>{{$bankDetail->bank_name}}</td>
        </tr>
        <tr class="item">
            <td>Account #</td>
            <td>{{$bankDetail->account_no}}</td>
        </tr>
        <tr class="item">
            <td>Short code</td>
            <td>{{$bankDetail->short_code}}</td>
        </tr>
        <tr class="item">
            <td>Phone #</td>
            <td>{{$bankDetail->phone}}</td>
        </tr>
        <tr class="item">
            <td>Zip</td>
            <td>{{$bankDetail->zip}}</td>
        </tr>
        <tr class="item">
            <td>State</td>
            <td>{{$bankDetail->state}}</td>
        </tr>
        <tr class="item">
            <td>Country</td>
            <td>{{$bankDetail->country}}</td>
        </tr>
        <tr class="total">
            <td></td>
            <td>Invoice Amount: {{$invoice->amount}}</td>
        </tr>
    </table>
    <p><i>---To generate invoice you agree with term's and conditions.</i></p>
</div>
</body>
</html>
