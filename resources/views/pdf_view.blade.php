<html>

<body style="background-color:#e2e1e0;font-family: Open Sans, sans-serif;font-size:100%;font-weight:400;line-height:1.4;color:#000;">
<table style="max-width:670px;margin:0px auto 10px;background-color:#fff;padding:20px 50px 0px 50px;-webkit-border-radius:3px;-moz-border-radius:3px;border-radius:3px;-webkit-box-shadow:0 1px 3px rgba(0,0,0,.12),0 1px 2px rgba(0,0,0,.24);-moz-box-shadow:0 1px 3px rgba(0,0,0,.12),0 1px 2px rgba(0,0,0,.24);box-shadow:0 1px 3px rgba(0,0,0,.12),0 1px 2px rgba(0,0,0,.24); border-top: solid 10px darkgoldenrod;">
    <thead>
    <tr>
        <th style="text-align:left;font-weight:400;">PayKamsy Invoice</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td style="height:5px;"></td>
    </tr>
    <tr>
        <td colspan="2" style="border: solid 1px #ddd; padding:10px 20px;">
            <p style="font-size:14px;margin:0 0 6px 0;"><span style="font-weight:bold;display:inline-block;min-width:150px">Invoice status</span><b style="color:green;font-weight:normal;margin:0">{{$invoice->invoice_status}}</b></p>
            <p style="font-size:14px;margin:0 0 6px 0;"><span style="font-weight:bold;display:inline-block;min-width:146px">Invoice No</span> {{$invoice->invoice_no}}</p>
            <p style="font-size:14px;margin:0 0 6px 0;"><span style="font-weight:bold;display:inline-block;min-width:146px">Invoice Name</span> {{$invoice->name}}</p>
            <p style="font-size:14px;margin:0 0 6px 0;"><span style="font-weight:bold;display:inline-block;min-width:146px">Creat date</span> {{date('d-m-Y', strtotime($invoice->created_at))}}</p>
            <p style="font-size:14px;margin:0 0 6px 0;"><span style="font-weight:bold;display:inline-block;min-width:146px">Due date</span> {{date('d-m-Y', strtotime($invoice->expiry_date))}}</p>
            <p style="font-size:14px;margin:0 0 0 0;"><span style="font-weight:bold;display:inline-block;min-width:146px">Total amount</span> Rs. 6000.00</p>
        </td>
    </tr>
    <tr>
        <td style="height:5px;"></td>
    </tr>
    <tr>
        <td style="width:10%;padding-left:20px;vertical-align:top">
            <p style="margin:0 0 5px 0;padding:0;font-size:14px;"><span style="display:block;font-weight:bold;font-size:13px">Name</span> {{$user->full_name}}</p>
            <p style="margin:0 0 5px 0;padding:0;font-size:14px;"><span style="display:block;font-weight:bold;font-size:13px;">Email</span> palash@gmail.com</p>
            <p style="margin:0 0 5px 0;padding:0;font-size:14px;"><span style="display:block;font-weight:bold;font-size:13px;">Phone</span> {{$user->phone}}</p>
            <p style="margin:0 0 5px 0;padding:0;font-size:14px;"><span style="display:block;font-weight:bold;font-size:13px;">Address</span> {{$user->address}}</p>
        </td>
        <td style="width:10%;padding-right:20px;vertical-align:top">
            <p style="margin:0 0 5px 0;padding:0;font-size:14px;"><span style="display:block;font-weight:bold;font-size:13px;">City</span> {{$user->city}}</p>
            <p style="margin:0 0 5px 0;padding:0;font-size:14px;"><span style="display:block;font-weight:bold;font-size:13px;">state</span> {{$user->state}}</p>
            <p style="margin:0 0 5px 0;padding:0;font-size:14px;"><span style="display:block;font-weight:bold;font-size:13px;">Zip</span> {{$user->zip}}</p>
            <p style="margin:0 0 5px 0;padding:0;font-size:14px;"><span style="display:block;font-weight:bold;font-size:13px;">Country</span> {{$user->country}}</p>
        </td>
    </tr>
    <tr>
        <td colspan="2" style="font-size:20px;padding:0px 15px 0 15px;">Description</td>

    </tr>
    <tr>
        <td colspan="2" style="font-size:12px;padding:0px 15px 0 15px;">{{$invoice->detail}}</td>
    </tr>
    <tr><td colspan="2" style="font-size:20px;padding:5px 15px 0 15px;">PayKamsy Bank detail</td></tr>
    <tr>
        <td  style="padding-left:15px;">
            <p style="font-size:12px;margin:0;padding:5px;border:solid 1px #ddd;font-weight:bold;">
                <span style="display:block;font-size:13px;font-weight:normal;">Bank Name</span> {{$bankDetail->bank_name}}
            </p>
            <p style="font-size:12px;margin:0;padding:5px;border:solid 1px #ddd;font-weight:bold;"><span style="display:block;font-size:13px;font-weight:normal;">Phone No</span> {{$bankDetail->phone}}</p>
            <p style="font-size:12px;margin:0;padding:5px;border:solid 1px #ddd;font-weight:bold;"><span style="display:block;font-size:13px;font-weight:normal;">city</span> {{$bankDetail->city}}</p>
            <p style="font-size:12px;margin:0;padding:5px;border:solid 1px #ddd;font-weight:bold;"><span style="display:block;font-size:13px;font-weight:normal;">Zip</span> {{$bankDetail->zip}}</p>
        </td>
        <td  style="padding-right: 15px">
            <p style="font-size:12px;margin:0;padding:5px;border:solid 1px #ddd;font-weight:bold;"><span style="display:block;font-size:13px;font-weight:normal;">Account No</span>{{$bankDetail->account_no}}</p>
            <p style="font-size:12px;margin:0;padding:5px;border:solid 1px #ddd;font-weight:bold;"><span style="display:block;font-size:13px;font-weight:normal;">Short code</span>{{$bankDetail->short_code}}</p>
            <p style="font-size:12px;margin:0;padding:5px;border:solid 1px #ddd;font-weight:bold;"><span style="display:block;font-size:13px;font-weight:normal;">State</span> {{$bankDetail->state}}</p>
            <p style="font-size:12px;margin:0;padding:5px;border:solid 1px #ddd;font-weight:bold;"><span style="display:block;font-size:13px;font-weight:normal;">Country</span> {{$bankDetail->country}}</p>
        </td>
    </tr>
    @if($invoice->invoice_for == '0')
        <tr><td colspan="2" style="font-size:20px;padding:10px 15px 0 15px;">Invoice for: <span style="color: green">Goods</span></td></tr>
        <td  style="padding-left: 15px">
            <p style="font-size:12px;margin:0;padding:5px;border:solid 1px #ddd;font-weight:bold;"><span style="display:block;font-size:13px;font-weight:normal;">Brand name:</span>{{$gsd->brand_name}}</p>
            <p style="font-size:12px;margin:0;padding:5px;border:solid 1px #ddd;font-weight:bold;"><span style="display:block;font-size:13px;font-weight:normal;">Model No:</span>{{$gsd->model_no}}</p>
            <p style="font-size:12px;margin:0;padding:5px;border:solid 1px #ddd;font-weight:bold;"><span style="display:block;font-size:13px;font-weight:normal;">Serial No:</span>{{$gsd->serial_no}}</p>
        </td>
        <td  style="padding-right: 15px">
            <p style="font-size:12px;margin:0;padding:5px;border:solid 1px #ddd;font-weight:bold;"><span style="display:block;font-size:13px;font-weight:normal;">Manufacture year:</span>{{$gsd->year_of_manufacture}}</p>
            <p style="font-size:12px;margin:0;padding:5px;border:solid 1px #ddd;font-weight:bold;"><span style="display:block;font-size:13px;font-weight:normal;">Product condition:</span>{{$gsd->product_condition}}</p>
            <p style="font-size:12px;margin:0;padding:5px;border:solid 1px #ddd;font-weight:bold;"><span style="display:block;font-size:13px;font-weight:normal;">Extra links:</span>{{$gsd->extra_link}}</p>
        </td>
    @endif
    @if($invoice->invoice_for == '1')
        <tr><td colspan="2" style="font-size:20px;padding:10px 15px 0 15px;">Invoice for: <span style="color: green">Services</span></td></tr>
        <td  style="padding-left: 15px">
            <p style="font-size:12px;margin:0;padding:5px;border:solid 1px #ddd;font-weight:bold;"><span style="display:block;font-size:13px;font-weight:normal;">Service title:</span>{{$gsd->service_title}}</p>
        </td>
        <td  style="padding-right: 15px">
            <p style="font-size:12px;margin:0;padding:5px;border:solid 1px #ddd;font-weight:bold;"><span style="display:block;font-size:13px;font-weight:normal;">Service description:</span>{{$gsd->service_description}}</p>
        </td>
    @endif
    @if($invoice->delivery_require == 'yes')
        <tr><td colspan="2" style="font-size:20px;padding:10px 15px 0 15px;">Delivery options Require: <span style="color: green;">Yes</span></td></tr>
        <td style="padding-left: 15px; vertical-align:top">
            <p style="font-size:12px;margin:0;padding:5px;border:solid 1px #ddd;font-weight:bold;"><span style="display:block;font-size:13px;font-weight:normal;">Delivery Days:</span>{{$invoice->delivery_days}}</p>
            <p style="font-size:12px;margin:0;padding:5px;border:solid 1px #ddd;font-weight:bold;"><span style="display:block;font-size:13px;font-weight:normal;">Delivery Days:</span>{{$invoice->delivery_days}}</p>
        </td>
        <td  style="padding-right: 15px; vertical-align:top">
            <p style="font-size:12px;margin:0;padding:5px;border:solid 1px #ddd;font-weight:bold;"><span style="display:block;font-size:13px;font-weight:normal;">Shipment cost:</span>{{$invoice->shipment_cost}}</p>
        </td>
    @endif

    @if($invoice->delivery_require == 'no')
        <tr><td colspan="2" style="font-size:20px;padding:10px 15px 0 15px;">Delivery options Require: <span style="color: green;">No</span></td></tr>
        <td  style="padding-left: 15px">
            <p style="font-size:12px;margin:0;padding:5px;border:solid 1px #ddd;font-weight:bold;"><span style="display:block;font-size:13px;font-weight:normal;">Contract end in day's: </span>{{$invoice->end_contract}}</p>
        </td>
        <td  style="padding-right: 15px">
            <p style="font-size:12px;margin:0;padding:5px;border:solid 1px #ddd;font-weight:bold;"><span style="display:block;font-size:13px;font-weight:normal;">Who pay service charges:</span>{{$invoice->pay_service_charges}}</p>
        </td>
    @endif

    </tbody>
    <tfooter>
        <tr>
            <td colspan="2" style="font-style: italic; font-size:10px;padding:50px 15px 0 15px;">
                ---To generate invoice you agree with term's and conditions.<br><br>
            </td>
        </tr>
    </tfooter>
</table>
</body>

</html>
