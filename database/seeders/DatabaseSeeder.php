<?php

namespace Database\Seeders;
use App\Models\UserModel;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        UserModel::factory()->create([
            'name'=>'Admin',
            'email'=>'admin@admin.com',
            'address'=>'admin',
            'password'=>md5('admin'),
            'phone' => '1234',
            'user_rights' => '',
            'user_type'=>'admin'
        ]);
    }
}
