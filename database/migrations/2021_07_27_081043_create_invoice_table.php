<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInvoiceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('invoice', function (Blueprint $table) {
            $table->id();
            $table->string('invoice_no')->unique();
            $table->string('name')->nullable();
            $table->date('expiry_date')->nullable();
            $table->string('bank_name')->nullable();
            $table->double('amount')->nullable();
            $table->string('detail')->nullable();
            $table->integer('invoice_for')->comment('0=goods,1=services')->nullable();
            $table->string('invoice_gs_detail')->nullable();
            $table->string('delivery_require')->comment('yes, no')->nullable();
            $table->string('shipment_mode')->nullable();
            $table->string('delivery_days')->nullable();
            $table->double('shipment_cost')->nullable();
            $table->string('delivery_option')->nullable();
            $table->string('invoice_type')->comment("guest, user")->nullable();
            $table->string('invoice_status')->comment("pending, in-process, complete, canceled")->default("pending")->nullable();
            $table->string('guest_user_detail')->nullable();
            $table->string('guest_bank_detail')->nullable();
            $table->string('invoice_password')->nullable();
            $table->unsignedBigInteger('app_user_id');
            $table->timestamps();
//            $table->foreign('app_user_id')->references('id')->on('app_users')
//                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invoice');
    }
}
