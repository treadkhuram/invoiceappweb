<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInvoiceAmount extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoice_amount', function (Blueprint $table) {
            $table->id();
            $table->string('transaction_name')->nullable();
            $table->string('bank_name')->nullable();
            $table->double('paid_amount');
            $table->date('paid_date')->nullable();
            $table->string('reference_no')->nullable();
            $table->integer('verified')->default(0)->comment('0=not_verified, 1=verified');
            $table->unsignedBigInteger('invoice_id');
            $table->timestamps();
            $table->foreign('invoice_id')->references('id')->on('invoice');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invoice_amount');
    }
}
